%% 2-FFR robot
% inverse problem with assumed mode

clear
close all
clc

%% GEOMETRICAL PARAMETERS

% base
rAB = 0.04;
A1 = [-rAB/2;0];
A2 = [+rAB/2;0];

basepoints = [A1,A2];

% platform
lP = 0.04;
B1 = [-lP/2;0];
B2 = [+lP/2;0];
platpoints = [B1,B2];

rodbaseangle = -pi/2;

geometry.basepoints = basepoints;
geometry.platpoints = platpoints;
geometry.rodbaseangle = rodbaseangle;

%% MATERIAL PARAMETERS
r = 0.001;
E = 36.1*10^9;
I = 0.25*pi*r^4;
EI = E*I;


%% EXTERNAL LOADS
fd = [0;0];
fend = [0;0];
mend = 0;
wp = [mend;fend];
wd = [0;fend];

%% FORWARD PROBLEM

qpd = [-0.4;1];

%% SIMULATION
Nf = 4;
Phi = @(s) BaseFcnLegendre(s,1,Nf)';
Kad = integral(@(s) Phi(s)'*EI*Phi(s),0,1,'ArrayValued',true);

qa0 = qpd;
qe0 = [0;zeros(Nf-1,1);0;zeros(Nf-1,1)];
qp0 = [0;-0.4;0];
lambda0 = 0.00001*rand(2*2,1);
guess0 = [qa0;qe0;qp0;lambda0];

fun = @(guess) InverseMode2FFR(guess,geometry,Kad,qpd,wp,wd,Nf);

options = optimoptions('fsolve','SpecifyObjectiveGradient',true,'CheckGradients',true,'Display','iter-detailed','Maxiter',50,'Algorithm','levenberg-marquardt');
[sol,~,flag,~,jac] = fsolve(fun,guess0,options);


%% PLOT & SHAPE RECOVERY
[pos1,pos2] = pos2FFR(sol,geometry,Nf);
pplat = sol(1+2+2*Nf:2+2*Nf+2,1);
th = sol(2+2*Nf+3,1);
Plot2FFR(pos1,pos2,pplat,th,geometry,max(sol(1,1),sol(2,1)))
