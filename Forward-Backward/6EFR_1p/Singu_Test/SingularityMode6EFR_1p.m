function [flag1,flag2] = SingularityMode6EFR_1p(jac,Nfm,eul,rotparams)

Nf = 3*Nfm;

D = rot2twist(eul,rotparams);
jac(1+7*Nf+1+3:7*Nf+1+6,:) = D'*jac(1+7*Nf+1+3:7*Nf+1+6,:);

A1 = jac(1:7*Nf+1+6,1:6);
U1 = jac(1:7*Nf+1+6,1+6:6+7*Nf+1);
P1 = jac(1:7*Nf+1+6,1+6+7*Nf+1:6+7*Nf+1+6);

A2 = jac(1+7*Nf+1+6:7*Nf+1+6+5*7+1,1:6);
U2 = jac(1+7*Nf+1+6:7*Nf+1+6+5*7+1,1+6:6+7*Nf+1);
P2 = jac(1+7*Nf+1+6:7*Nf+1+6+5*7+1,1+6+7*Nf+1:6+7*Nf+1+6);

G = jac(1:7*Nf+1+6,1+6+7*Nf+1+6:6+6+7*Nf+1+7*5+1);
Z = null(G');

T1 = [Z'*A1,Z'*U1;
         A2,   U2];
     
T2 = [Z'*P1,Z'*U1; 
         P2,   U2];

flag1 = rcond(T1);
flag2 = rcond(T2);

end