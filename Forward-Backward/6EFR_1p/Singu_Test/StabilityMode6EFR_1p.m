function flag = StabilityMode6EFR_1p(jac,Nfm,eul,rotparams)
Nf = 3*Nfm;

D = rot2twist(eul,rotparams);
jac(1+7*Nf+1+3:7*Nf+1+6,:) = D'*jac(1+7*Nf+1+3:7*Nf+1+6,:);

U = jac(1:7*Nf+1+6,1+6:6+7*Nf+1);
P = jac(1:7*Nf+1+6,1+6+7*Nf+1:6+7*Nf+1+6);

G = jac(1:7*Nf+1+6,1+6+7*Nf+1+6:6+6+7*Nf+1+7*5+1);
Z = null(G');

H = [U,P];
Hr = Z'*H*Z;

[~,flag] = chol(Hr);

end