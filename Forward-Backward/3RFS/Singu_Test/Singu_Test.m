%% Singularity Test
% Assumed strain mode approach - forward/backward 3RFS robot
% 14 Dec 2021 - Federico Zaccaria

clear
close all
clc
%%
r = 0.001;
E = 210*10^9;
G = 80*10^9;
L = 1;
A = pi*r^2;
I = 0.25*pi*r^4;
J = 2*I;
EI = E*I;
GJ = G*J;
Kbt = diag([EI;EI;GJ]);
g = [0;0;0];
rho = 7800;
f = rho*A*g;
mp = 0.00;
g = -9.81;
fext = mp*[0;0;g];
mext = [0;0;0];
wp = [mext;fext];
wd = [zeros(3,1);f]; % distributed wrench


%% Inverse problem data
rotparams = 'XYZ';
qpd = [0.05;-0.1;0.8];

%% GEOMETRY

rb = 0.4;
rp = 0.2;

th1 =   0*pi/180;
th2 = 120*pi/180;
th3 = 240*pi/180;

baseangles = [th1,th2,th3];

A1 = Rz(th1)*[0;rb;0]; % WRT base frame
A2 = Rz(th2)*[0;rb;0];
A3 = Rz(th3)*[0;rb;0];

basepoints = [A1,A2,A3];

B1 = Rz(th1)*[0;rp;0]; % WRT platform frame
B2 = Rz(th2)*[0;rp;0];
B3 = Rz(th3)*[0;rp;0];

platformpoints = [B1,B2,B3];

geometry.basepoints = basepoints;
geometry.platpoints = platformpoints;
geometry.baseangles = baseangles;
geometry.rotparams = rotparams;

%% Solution
Nf = 4;
qa0 = zeros(3,1);
qe0 = zeros(3*3*Nf,1);
qp0 = [qpd;zeros(3,1)];
lambda0 = zeros(3*3,1);
guess0 = [qa0;qe0;qp0;lambda0];
options = optimoptions('fsolve','Algorithm','trust-region','display','iter-detailed','Maxiter',50,'SpecifyObjectiveGradient',true,'CheckGradients',true);

M = @(s) PhiMatr(s,1,Nf);

Kad = integral(@(s) M(s)'*Kbt*M(s),0,1,'ArrayValued',true);

fun = @(guess) InverseMode3RFS(guess,geometry,L,Kad,qpd,wp,wd,Nf);

[sol,~,flag,~,jac] = fsolve(fun,guess0,options);

%% PLOT & SHAPE RECOVERY
posbeams = pos3RFS(sol,geometry,Nf,L);

qa = sol(1:3,1);
pplat = sol(1+3+3*3*Nf:3+3*3*Nf+3,1);
[Rplat,~,~,~] = rotationParametrization(sol(1+3+3*3*Nf+3:3+3*3*Nf+6,1),rotparams);

h = Plot3RFS(geometry,qa,posbeams,pplat,Rplat);

%% MOVE
options = optimoptions('fsolve','display','off','Algorithm','trust-region','Maxiter',5,'SpecifyObjectiveGradient',true,'CheckGradients',false);

i = 1;
i_max = 200;
while i<i_max && flag>0
    guess0 = sol;
    qpd = qpd-[0;0;0.01];
    fun = @(guess) InverseMode3RFS(guess,geometry,L,Kad,qpd,wp,wd,Nf);
    [sol,~,flag,~,jac] = fsolve(fun,guess0,options);
    orient = sol(1+3+3*3*Nf+3:3+3*3*Nf+3+3,1);
    jac = full(jac);
    yy(i) = i;
    flags(i) = StabilityMode3RFS(jac,Nf,orient,rotparams);
    [t1,t2] = SingularityMode3RFS(jac,Nf,orient,rotparams);
    flagt1(i) = t1;
    flagt2(i) = t2;
    invjac(i) = norm(inv(jac),Inf);
    i = i+1    

end

posbeams = pos3RFS(sol,geometry,Nf,L);

qa = sol(1:3,1);
pplat = sol(1+3+3*3*Nf:3+3*3*Nf+3,1);
[Rplat,~,~,~] = rotationParametrization(sol(1+3+3*3*Nf+3:3+3*3*Nf+6,1),rotparams);

h = Plot3RFS(geometry,qa,posbeams,pplat,Rplat);

%% RESULTS
figure()
subplot(1,2,1)
yyaxis left
plot(yy,flags)
yyaxis right
semilogy(yy,flagt2)
grid on
subplot(1,2,2)
yyaxis left
semilogy(yy,flagt1)
yyaxis right
semilogy(yy,invjac)
grid on