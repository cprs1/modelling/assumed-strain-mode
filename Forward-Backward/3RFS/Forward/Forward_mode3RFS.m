%% Forward Geometrico-Static problem
% Assumed strain mode approach 3RFS robot
% 14 Dec 2021 Federico Zaccaria
clear
close all
clc
%%
r = 0.001;
E = 210*10^9;
G = 80*10^9;
L = 1;
A = pi*r^2;
I = 0.25*pi*r^4;
J = 2*I;
EI = E*I;
GJ = G*J;
Kbt = diag([EI;EI;GJ]);
g = [0;0;0];
rho = 7800;
f = rho*A*g;
fext = [0;0;0];
mext = [0;0;0];
wp = [mext;fext];
wd = [zeros(3,1);f]; % distributed wrench


%% Forward problem data

qad = [-45;-65;-50]*pi/180;

%% GEOMETRY

rb = 0.4;
rp = 0.2;

th1 =   0*pi/180;
th2 = 120*pi/180;
th3 = 240*pi/180;

baseangles = [th1,th2,th3];

A1 = Rz(th1)*[0;rb;0]; % WRT base frame
A2 = Rz(th2)*[0;rb;0];
A3 = Rz(th3)*[0;rb;0];

basepoints = [A1,A2,A3];

B1 = Rz(th1)*[0;rp;0]; % WRT platform frame
B2 = Rz(th2)*[0;rp;0];
B3 = Rz(th3)*[0;rp;0];

platformpoints = [B1,B2,B3];

geometry.basepoints = basepoints;
geometry.platpoints = platformpoints;
geometry.baseangles = baseangles;


%% Solution
Nf = 4;
qa0 = qad;
qe0 = zeros(3*3*Nf,1);
qp0 = [0;0;0.8];
qporient = zeros(3,1);
lambda0 = zeros(3*3,1);
guess0 = [qa0;qe0;qp0;qporient;lambda0];
options = optimoptions('fsolve','Algorithm','trust-region','display','iter-detailed','Maxiter',50,'SpecifyObjectiveGradient',true,'CheckGradients',true);

M = @(s) PhiMatr(s,1,Nf);

Kad = integral(@(s) M(s)'*Kbt*M(s),0,1,'ArrayValued',true);

fun = @(guess) ForwardMode3RFS(guess,geometry,L,Kad,qad,wp,wd,Nf);

[sol,~,flag,~,jac] = fsolve(fun,guess0,options);
% sol = guess0
%% PLOT & SHAPE RECOVERY
posbeams = pos3RFS(sol,geometry,Nf,L);

qa = sol(1:3,1);
pplat = sol(1+3+3*3*Nf:3+3*3*Nf+3,1);
roll = sol(3+3*3*Nf+3+1,1);
pitch = sol(3+3*3*Nf+3+2,1);
yaw = sol(3+3*3*Nf+3+3,1);
Rplat = Rz(roll)*Ry(pitch)*Rz(yaw);

Plot3RFS(geometry,qa,posbeams,pplat,Rplat);