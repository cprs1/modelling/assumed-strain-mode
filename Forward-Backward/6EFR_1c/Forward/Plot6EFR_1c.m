function  Plot6EFR_1c(geometry,posbeams,pplat,Rplat,pdisk,Rdisk,r)

basepoints = geometry.basepoints;
baseangles = geometry.baseangles;
platformpoints = geometry.platpoints;
diskpoints = geometry.diskpoints;

posbeamsb = posbeams(1:3*6,:);
posbeamsd = posbeams(1+3*6:6*6,:);
posbeampassive = posbeams(1+6*6:6*6+3,:);

% plot fixed frames
yvect = [0;1;0]/5;
xvect = [1;0;0]/5;
zvect = [0;0;1]/5;
quiver3(0,0,0,xvect(1),xvect(2),xvect(3),'Color','r','LineWidth',1.5)
hold on
quiver3(0,0,0,yvect(1),yvect(2),yvect(3),'Color','b','LineWidth',1.5)
quiver3(0,0,0,zvect(1),zvect(2),zvect(3),'Color','k','LineWidth',1.5)

platpoint = zeros(3,7);

for i = 1:6
   platpoint(:,i) = pplat + Rplat*platformpoints(:,i);
   plot3(basepoints(1,i),basepoints(2,i),basepoints(3,i),'bd','LineWidth',1.5)
   plot3(platpoint(1,i),platpoint(2,i),platpoint(3,i),'rs','LineWidth',1.5) 
   Ri0 = Rz(baseangles(i));
   xlocal = Ri0*xvect;
   ylocal = Ri0*yvect;
   zlocal = Ri0*zvect;
%    quiver3(basepoints(1,i),basepoints(2,i),basepoints(3,i),xlocal(1),xlocal(2),xlocal(3),'Color','r','LineWidth',1.5)
%    quiver3(basepoints(1,i),basepoints(2,i),basepoints(3,i),ylocal(1),ylocal(2),ylocal(3),'Color','b','LineWidth',1.5)
%    quiver3(basepoints(1,i),basepoints(2,i),basepoints(3,i),zlocal(1),zlocal(2),zlocal(3),'Color','k','LineWidth',1.5)
   yi1 = posbeamsb(1+3*(i-1):3*i,:);
   plot3(yi1(1,:),yi1(2,:),yi1(3,:),'k','LineWidth',1.5)
   yi2 = posbeamsd(1+3*(i-1):3*i,:);
   plot3(yi2(1,:),yi2(2,:),yi2(3,:),'k','LineWidth',1.5)
   diskpoint = pdisk + Rdisk*diskpoints(:,i);
   
   [xc,yc,zc] = Circles(diskpoint,Rdisk,r/20,50);
   plot3(xc,yc,zc,'r','LineWidth',1.5)
%    plot3(diskpoint(1),diskpoint(2),diskpoint(3),'ro','LineWidth',1.5) 
    
end

% Passive leg
   i = 7;
   platpoint(:,i) = pplat + Rplat*diskpoints(:,i);
   plot3(basepoints(1,i),basepoints(2,i),basepoints(3,i),'bd','LineWidth',1.5)
   plot3(pdisk(1),pdisk(2),pdisk(3),'rs','LineWidth',1.5) 
   yi1 = posbeampassive;
   plot3(yi1(1,:),yi1(2,:),yi1(3,:),'Color',[0.8500 0.3250 0.0980],'LineWidth',1.5)

% plot intermediate disk
[xd,yd,zd] = Circles(pdisk,Rdisk,r,100);
plot3(xd,yd,zd,'LineWidth',1.5)

% plot platform frame
xplat = Rplat*xvect;
yplat = Rplat*yvect;
zplat = Rplat*zvect;
quiver3(pplat(1),pplat(2),pplat(3),xplat(1),xplat(2),xplat(3),'Color','r','LineWidth',1.5)
quiver3(pplat(1),pplat(2),pplat(3),yplat(1),yplat(2),yplat(3),'Color','b','LineWidth',1.5)
quiver3(pplat(1),pplat(2),pplat(3),zplat(1),zplat(2),zplat(3),'Color','k','LineWidth',1.5)

% plot platform
line([platpoint(1,1),platpoint(1,2)],[platpoint(2,1),platpoint(2,2)],[platpoint(3,1),platpoint(3,2)],'LineWidth',1.5)
line([platpoint(1,2),platpoint(1,3)],[platpoint(2,2),platpoint(2,3)],[platpoint(3,2),platpoint(3,3)],'LineWidth',1.5)
line([platpoint(1,3),platpoint(1,4)],[platpoint(2,3),platpoint(2,4)],[platpoint(3,3),platpoint(3,4)],'LineWidth',1.5)
line([platpoint(1,4),platpoint(1,5)],[platpoint(2,4),platpoint(2,5)],[platpoint(3,4),platpoint(3,5)],'LineWidth',1.5)
line([platpoint(1,5),platpoint(1,6)],[platpoint(2,5),platpoint(2,6)],[platpoint(3,5),platpoint(3,6)],'LineWidth',1.5)
line([platpoint(1,6),platpoint(1,1)],[platpoint(2,6),platpoint(2,1)],[platpoint(3,6),platpoint(3,1)],'LineWidth',1.5)

axis equal
axis([-1 1 -1 1 -1 1])
grid on

end