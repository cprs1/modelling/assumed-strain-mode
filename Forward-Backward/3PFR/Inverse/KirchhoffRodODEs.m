function yd = KirchhoffRodODEs(~,y,L,EI,wd)
% y = [px py th nx ny mz] in global frame
yd = zeros(6,1);
yd(1) = L*cos(y(3));
yd(2) = L*sin(y(3));
yd(3) = L*y(6)/(EI);
yd(4) = L*wd(2);
yd(5) = L*wd(3);
yd(6) = L*(-yd(1)*y(5) + yd(2)*y(4)-wd(1));
end