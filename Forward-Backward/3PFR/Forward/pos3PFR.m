function [pos1,pos2,pos3] = pos3PFR(guess,geometry,Nf,L)
Nsh = 100;
basepoints = geometry.basepoints;
baseangles = geometry.baseangles;
qa = guess(1:3,1);
qe = guess(1+3:3+3*Nf,1);

% extract variables
p01 = basepoints(:,1) + Rz(baseangles(1))*([0;qa(1)]);
th01 = baseangles(1)-pi/2;
qe1 = qe(1+Nf*(1-1):1*Nf,1);
% integrate
y01 = [p01;th01];
fun1 = @(s,y) OdeFunReconstruct(s,y,qe1,Nf,L);
[s1,y1] = ode45(fun1,[0,1],y01);

% spline results
sspan1 = linspace(0,L,Nsh);
pos1 = spline(L*s1,y1(:,1:2)',sspan1);


%% BEAM 2 INTEGRATION
% extract variables
p02 = basepoints(:,2) + Rz(baseangles(2))*([0;qa(2)]);
th02 = baseangles(2)-pi/2;
qe2 = qe(1+Nf*(2-1):2*Nf,1);
% integrate
y02 = [p02;th02];
fun1 = @(s,y) OdeFunReconstruct(s,y,qe2,Nf,L);
[s2,y2] = ode45(fun1,[0,1],y02);

% spline results
sspan2 = linspace(0,L,Nsh);
pos2 = spline(L*s2,y2(:,1:2)',sspan2);

%% BEAM 3 INTEGRATION
% extract variables
p03 = basepoints(:,3) + Rz(baseangles(3))*([0;qa(3)]);
th03 = baseangles(3)-pi/2;
qe3 = qe(1+Nf*(3-1):3*Nf,1);

% integrate
y03 = [p03;th03];
fun3 = @(s,y) OdeFunReconstruct(s,y,qe3,Nf,L);
[s3,y3] = ode45(fun3,[0,1],y03);

% spline
sspan3 = linspace(0,L,Nsh);
pos3 = spline(L*s3,y3(:,1:2)',sspan3);
end