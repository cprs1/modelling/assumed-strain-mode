function Plot2FF(pos1,pos2,pP,thP,geometry,L)
basepoints = geometry.basepoints;
platformpoints = geometry.platpoints;

A1 = basepoints(:,1);
A2 = basepoints(:,2);
B1 = platformpoints(:,1);
B2 = platformpoints(:,2);

B1f = Rz(thP)*B1+pP; %wrt fixed frame
B2f = Rz(thP)*B2+pP;


plot(A1(1),A1(2),'bs','LineWidth',1) % base points
hold on
plot(A2(1),A2(2),'bs','LineWidth',1)
grid on
plot(B1f(1),B1f(2),'ks','LineWidth',1) % platform points
plot(B2f(1),B2f(2),'ks','LineWidth',1)


line([B1f(1),B2f(1)],[B1f(2),B2f(2)],'Color','b','LineWidth',2)

plot(pos1(1,:),pos1(2,:),'k','LineWidth',2) % flexible beams
plot(pos2(1,:),pos2(2,:),'k','LineWidth',2)

x_ax = [1;0]*0.2*L; % fixed frame
y_ax = [0;1]*0.2*L;

quiver(0,0,x_ax(1),x_ax(2),'b','LineWidth',1) 
quiver(0,0,y_ax(1),y_ax(2),'b','LineWidth',1)

x_ax_p = Rz(thP)*x_ax; % platform frame
y_ax_p = Rz(thP)*y_ax;

quiver(pP(1),pP(2),x_ax_p(1),x_ax_p(2),'r--','LineWidth',1) 
quiver(pP(1),pP(2),y_ax_p(1),y_ax_p(2),'r--','LineWidth',1)

axis equal
axis(1.1*[-L L -L 0.1*L])

end