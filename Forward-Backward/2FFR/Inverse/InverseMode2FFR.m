function [eq,gradeq] = InverseMode2FFR(guess,geometry,Kee,qpd,wp,wd,Nf)

% extract common variabes
basepoints = geometry.basepoints;
platpoints = geometry.platpoints;
rodbaseangle = geometry.rodbaseangle;

qa = guess(1:2,1);
qe = guess(1+2:2+2*Nf,1);
qp = guess(1+2+2*Nf:2+2*Nf+3,1);
lambda = guess(1+2+2*Nf+3:2+2*Nf+3+4,1);

pplat = qp(1:2,1);
thplat = qp(3,1);
Rp = Rz(thplat);
dRpdthp = Rz(thplat+pi/2);

C = [zeros(1,2);eye(2)];

% initialize arrays
beameq = zeros(2*Nf,1);
dbeamdqa = zeros(2*Nf,2);
dbeamdqe = zeros(2*Nf,2*Nf);
dbeamdlambd = zeros(2*Nf,4);
geomconstr = zeros(4,1);
dconsdqa =  zeros(4,2);        
dconsdqe = zeros(4,2*Nf);    
dconsdqp = zeros(4,3);

equilibrium = [wp(2:3,1);wp(1)];
dequidqa = zeros(3,2);
dequidqe = zeros(3,2*Nf);
dequidqp = zeros(3,3);
dequidlambd = zeros(3,4);

for i = 1:2

%% INTEGRATION

% extract variables
p0 = basepoints(:,i);
pp = Rp*platpoints(:,i);
pLp = pplat + pp;
th0 = rodbaseangle;
qei = qe(1+Nf*(i-1):i*Nf,1);
lambdai = lambda(1+2*(i-1):2*i,1);
wrench = C*lambdai;
Li = qa(i,1);

% integrate forward recursion
y0F = [p0;th0;zeros(2+2*Nf,1);0;zeros(Nf,1)];
funF = @(s,y) OdefunAssumedForward(s,y,qei,Nf,Li,'variable');
[~,y] = ode45(funF,[0,1],y0F);
ygeom = y(end,:)';

% extract results
pL = ygeom(1:2,1);
thL = ygeom(3,1);
dpxdqeL = ygeom(1+5:5+Nf,1)';
dpydqeL = ygeom(1+5+Nf:5+2*Nf,1)';
dpLdqa = ygeom(4:5,1);
dthLdqa = ygeom(1+5+2*Nf,1);
dpLdqe = [dpxdqeL;dpydqeL];
dthLdqe = ygeom(1+5+2*Nf+1:end,1)'; 

% integrate backward recursion
y0B = [wrench;zeros(Nf,1);zeros(3+Nf,1);zeros(3*Nf+Nf*Nf,1);reshape(eye(3),3*3,1);zeros(3*Nf,1)];
funbackward = @(s,y) OdefunAssumeBackward(s,y,qei,wd,Nf,Li,'variable');
[~,y] = ode45(funbackward,[1,0],y0B);
yforces = y(end,:)';
       
% extract results
Qc =   yforces(1+3:3+Nf,1);
dQcdqa = yforces(1+3+Nf+3:3+Nf+3+Nf,1);
dQcdqe = reshape(yforces(1+3+Nf+3+Nf+3*Nf:3+Nf+3+Nf+3*Nf+Nf*Nf,1),Nf,Nf);
dQcdw0 = reshape(yforces(1+3+Nf+3+Nf+3*Nf+Nf*Nf+3*3:3+Nf+3+Nf+3*Nf+Nf*Nf+3*3+3*Nf,1),Nf,3);

% platform equibribrium contributions wrench = C*lambdai
Rtip = Rz(thL);
dRtipdth = Rz(thL+pi/2);
n = -Rtip*wrench(2:3);
m = -wrench(1) +n(2)*pp(1,1) -n(1)*pp(2,1);

dndqa = -dRtipdth*wrench(2:3)*dthLdqa;
dndqe = -dRtipdth*wrench(2:3)*dthLdqe;
dndlambd = -Rtip;
dmdqa = +dndqa(2)*pp(1,1) -dndqa(1)*pp(2,1);
dmdqe = +pp(1,1)*dndqe(2,:) -pp(2,1)*dndqe(1,:);
dppdthp = dRpdthp*platpoints(:,i);
dmdthp = +n(2)*dppdthp(1,1) -n(1)*dppdthp(2,1);
dmdlambd =  +pp(1,1)*dndlambd(2,:) -pp(2,1)*dndlambd(1,:);

%% EQUATIONS
% equilibrium equations
beameq(1+Nf*(i-1):Nf*i,1) = Li*Kee*qei+Qc;
dbeamdqa(1+Nf*(i-1):Nf*i,i) = Kee*qei + dQcdqa;      
dbeamdqe(1+Nf*(i-1):Nf*i,1+Nf*(i-1):Nf*i) = Li*Kee + dQcdqe;        
dbeamdlambd(1+Nf*(i-1):Nf*i,1+2*(i-1):2*i) = dQcdw0*C;

% geometric constraints
geomconstr(1+2*(i-1):2*i,1) = pL-pLp;
dconsdqa(1+2*(i-1):2*i,i) =  dpLdqa;        
dconsdqe(1+2*(i-1):2*i,1+Nf*(i-1):Nf*i) = dpLdqe;       
dconsdqp(1+2*(i-1):2*i,:) = -[eye(2),dppdthp];
         
% platform equilibrium

equilibrium = equilibrium + [n;m];
dequidqa(:,i) = [dndqa;dmdqa];
dequidqe(:,1+Nf*(i-1):Nf*i) = [dndqe;dmdqe];
dequidqp = dequidqp + [zeros(2,3);zeros(1,2),dmdthp];
dequidlambd(:,1+2*(i-1):2*i) = [dndlambd;dmdlambd];

% forward problem

inv = qp(2:3,1)-qpd;
gradinv = [zeros(2,1),eye(2)];
% collect

eq = [beameq;equilibrium;geomconstr;inv];

gradeq = [dbeamdqa,   dbeamdqe,      zeros(2*Nf,3), dbeamdlambd;
          dequidqa,   dequidqe,      dequidqp,      dequidlambd;
          dconsdqa,   dconsdqe,      dconsdqp,      zeros(4,4);
          zeros(2,2), zeros(2,2*Nf), gradinv,       zeros(2,4);];
end

