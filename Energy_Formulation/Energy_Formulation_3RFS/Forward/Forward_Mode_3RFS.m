%% Forward Geometrico-Static Problem
% weak form solved with Assumed Mode - energetic approach
% 3-RFS Robot

% 14-Dec-2021 Federico Zaccaria

%%
clear
close all
clc

r = 0.001;
E = 210*10^9;
G = 80*10^9;
L = 1;
A = pi*r^2;
I = 0.25*pi*r^4;
J = 2*I;
EI = E*I;
GJ = G*J;
Kbt = diag([EI;EI;GJ]);
g = [0;0;0];
rho = 7800;
fd = rho*A*g;
fext = [0;0;0];


%% Inverse problem data

qad = [-60;-60;-60]*pi/180;

%% GEOMETRY

rb = 0.4;
rp = 0.2;

th1 =   0*pi/180;
th2 = 120*pi/180;
th3 = 240*pi/180;

baseangles = [th1,th2,th3];

A1 = Rz(th1)*[0;rb;0]; % WRT base frame
A2 = Rz(th2)*[0;rb;0];
A3 = Rz(th3)*[0;rb;0];

basepoints = [A1,A2,A3];

B1 = Rz(th1)*[0;rp;0]; % WRT platform frame
B2 = Rz(th2)*[0;rp;0];
B3 = Rz(th3)*[0;rp;0];

platformpoints = [B1,B2,B3];

geometry.basepoints = basepoints;
geometry.platpoints = platformpoints;
geometry.baseangles = baseangles;


%% Solution
Nf = 4;
qa0 = qad;
qe0 = zeros(3*3*Nf,1);
qp0 = [0;0;0.8];
qporient = zeros(3,1);
lambda0 = zeros(3*3,1);
guess0 = [qa0;qe0;qp0;qporient;lambda0];
options = optimoptions('fsolve','Algorithm','trust-region','display','iter-detailed','Maxiter',50,'SpecifyObjectiveGradient',true,'CheckGradients',true);

M = @(s) PhiMatr(s,1,Nf);

Kad = integral(@(s) M(s)'*Kbt*M(s),0,1,'ArrayValued',true);

fun = @(guess) AssumedModeFGSPeqn3RFS(guess,Nf,geometry,Kad,qad,fd,fext,L);

[sol,~,flag,~,jac] = fsolve(fun,guess0,options);

%% PLOT & SHAPE RECOVERY
Nleg = 3;
Nftot = 3*Nf;
Nplot = 100;
qa = sol(1:Nleg,1); % actuated variables
qe = sol(Nleg+1:Nleg*(1+Nftot),1); % strains
qp = sol(Nleg*(1+Nftot)+1:Nleg*(1+Nftot)+6,1); % pose variables

s = linspace(0,L,Nplot);
posbeams = zeros(3*Nleg,Nplot);
for i = 1:Nleg
    Nftot = 3*Nf;
    qei = qe(1+Nftot*(i-1):Nftot*i,1);
    fun = @(s,y) OdefunAssumedMode(s,y,qei,fd,Nf,L);
    Sspan = [0,L];
    h0 = eul2quat([baseangles(i),0,qa(i)],'ZYX')';
    p0 = basepoints(:,i);
    Jh0 = zeros(4*Nftot,1);
    Jp0 = zeros(3*Nftot,1);
    Qw0 = zeros(Nftot,1);
    y01 = [h0;p0;Jh0;Jp0;Qw0];
    
    dhdqa0 = DerivativeMotorQuaternion([baseangles(i),0,qa(i)]);
    dpdqa0 = zeros(3,1);
    dJhdqa0 = zeros(4*Nftot,1);
    dJpdqa0 = zeros(3*Nftot,1);
    dQwdqa0 = zeros(Nftot,1);
    y02 = [dhdqa0;dpdqa0;dJhdqa0;dJpdqa0;dQwdqa0];
    
    dJhdqei0 = zeros(4*Nftot*Nftot,1);
    dJpdqei0 = zeros(3*Nftot*Nftot,1);
    dQwdqei0 = zeros(Nftot*Nftot,1);
    y03 = [dJhdqei0;dJpdqei0;dQwdqei0];
    
    y0 = [y01;y02;y03];

    [si,yi] = ode45(fun,Sspan,y0);
    pii = yi(:,5:7)';
    posbeams(1+3*(i-1):3*i,:) = spline(si,pii,s);

    nn = numel(yi(1,:));
end 
pplat = qp(1:3);
Rplat = Rz(qp(4))*Ry(qp(5))*Rz(qp(6));
Plot3RFS(geometry,qa,posbeams,pplat,Rplat);

