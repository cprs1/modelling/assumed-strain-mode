%% Inverse Geometrico Static Problem
% Strong form solved with shooting method
% RFRFR Robot


clear
close all
clc

%%
r = 0.001;
E = 210*10^9;
L = 1;
A = pi*r^2;
I = 0.25*pi*r^4;
Kbt = E*I;
g = [0;0];
rho = 7800;
f = rho*A*g;
fext = [0;.2];

wp = fext; % RFRFR no moments on EE
wd = [0;f]; % distributed wrench


%% Inverse problem data

pp = [0.2;0.8];
qpd = pp;

%% GEOMETRY
rAB = 0.4;

basepoints = [-rAB/2,+rAB/2;0,0];
geometry.basepoints = basepoints;


%% Solution
Nf = 4;
qa0 = [+pi;0];
qe0 = zeros(2*Nf,1);
qp0 = pp;
lambda0 = zeros(4,1);
guess0 = [qa0;qe0;qp0;lambda0];
options = optimoptions('fsolve','display','iter-detailed','Algorithm','trust-region','Maxiter',50,'SpecifyObjectiveGradient',true,'CheckGradients',false);

Phi = @(s) BaseFcnLegendre(s,1,Nf)';
Kad = integral(@(s) Phi(s)'*Kbt*Phi(s),0,1,'ArrayValued',true);

fun = @(guess) InverseModeForwBackRFRFSeqn(guess,geometry,Kad,L,wp,wd,qpd,Nf);
[sol,~,~,~,jac] = fsolve(fun,guess0,options);

%% PLOT

Nsh = 100;
s_span = linspace(0,L,Nsh);
posbeams = zeros(4,Nsh);
qe =  sol(1+2:2+2*Nf,1);
qa = sol(1:2);
qp = sol(1+2+2*Nf:2+2*Nf+2,1);

pplat = qp;

Lspan = [0,1];

for i = 1:2
    qai = qa(i);
    qei = qe(1+Nf*(i-1):Nf*i,1);
    fun = @(s,y) OdeFunReconstruct(s,y,qei,Nf,L);
    p0 = basepoints(:,i);
    th0 = qai;
    y0 = [p0;th0];
    [si,yi] = ode45(fun,Lspan,y0);
    posbeams(1+2*(i-1):2*i,:) = spline(L*si,yi(:,1:2)',s_span);
end
PlotRFRFR(posbeams(1:2,:),posbeams(3:4,:),L)
title('Assumed Mode')
axis equal

%% MOVE
options = optimoptions('fsolve','display','off','Algorithm','trust-region','Maxiter',10,'SpecifyObjectiveGradient',true,'CheckGradients',false);

i = 1;
flag = 1;
i_max = 1000;
while i<i_max && flag>0
    guess0 = sol;
    qpd = qpd-[0;0.002];
    fun = @(guess) InverseModeForwBackRFRFSeqn(guess,geometry,Kad,L,wp,wd,qpd,Nf);
    [sol,~,flag,~,jac] = fsolve(fun,guess0,options);
    jac = full(jac);
    yy(i) = i;
    flags(i) = StabilityModeRFRFR(jac,Nf);
    [t1,t2] = SingularityModeRFRFR(jac,Nf);
    flagt1(i) = t1;
    flagt2(i) = t2;
    i = i+1
end

%% PLOT 2
sol = guess0;
Nsh = 100;
s_span = linspace(0,L,Nsh);
posbeams = zeros(4,Nsh);
qe =  sol(1+2:2+2*Nf,1);
qa = sol(1:2);
qp = sol(1+2+2*Nf:2+2*Nf+2,1);

pplat = qp;

Lspan = [0,1];

for i = 1:2
    qai = qa(i);
    qei = qe(1+Nf*(i-1):Nf*i,1);
    fun = @(s,y) OdeFunReconstruct(s,y,qei,Nf,L);
    p0 = basepoints(:,i);
    th0 = qai;
    y0 = [p0;th0];
    [si,yi] = ode45(fun,Lspan,y0);
    posbeams(1+2*(i-1):2*i,:) = spline(L*si,yi(:,1:2)',s_span);
end
PlotRFRFR(posbeams(1:2,:),posbeams(3:4,:),L)
title('Assumed Mode')
axis equal

%% RESULTS
figure()
subplot(1,2,1)
yyaxis left
plot(yy,flags)
yyaxis right
semilogy(yy,flagt2)
grid on
subplot(1,2,2)
semilogy(yy,flagt1)
grid on