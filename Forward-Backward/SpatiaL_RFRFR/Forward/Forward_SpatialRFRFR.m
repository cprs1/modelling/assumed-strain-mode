%% Forward Geometrico-Static Problem
% spatial RFRFR robot
% assumed strain mode approach, forward-backward formulation
% Federico Zaccaria 05 Jan 2022

clear
close all
clc
%%
b = 0.010;
h = 0.002;
E = 25*10^9;
G = 10*10^9;
L = 1;
A = b*h;
Ix = b*h^3/12;
Iy = b^3*h/12;
J = b*h*(b^2+h^2)/12;
EIx = E*Ix;
EIy = E*Iy;
GJ = G*J;
Kbt = diag([EIx;EIy;GJ]);
g = [0;0;0];
rho = 1200;
f = rho*A*g;
mp = 0.00;
fext = mp*g;
wp = [zeros(3,1);fext];
wd = [zeros(3,1);f]; % distributed wrench


%% Forward problem data
rotparams = 'XYZ';
qad = [+60;-60]*pi/180; % motor angles

%% GEOMETRY

rAB = 0.4;
plane_dist = 0.005;
pA1 = [-plane_dist/2;-rAB/2;0];
pA2 = [+plane_dist/2;+rAB/2;0];
basepoints = [pA1,pA2];

pB1 = [-plane_dist/2;0;0];
pB2 = [+plane_dist/2;0;0];
platpoints = [pB1,pB2];

geometry.basepoints = basepoints;
geometry.platpoints = platpoints;
geometry.rotparams = rotparams;

%% Solution
Nf = 4;
qa0 = qad;
qe0 = zeros(2*3*Nf,1);
qp0 = [0;0;0.8;zeros(3,1)];
lambda0 = zeros(2*5+1,1);
guess0 = [qa0;qe0;qp0;lambda0];
options = optimoptions('fsolve','Algorithm','trust-region','display','iter-detailed','Maxiter',50,'SpecifyObjectiveGradient',true,'CheckGradients',true);

M = @(s) PhiMatr(s,1,Nf);

Kad = integral(@(s) M(s)'*Kbt*M(s),0,1,'ArrayValued',true);

fun = @(guess) ForwardModeSpatialRFRFR(guess,geometry,L,Kad,qad,wp,wd,Nf);

[sol,~,~,~,jac] = fsolve(fun,guess0,options);

%% PLOT & SHAPE RECOVERY
posbeams = posSpatialRFRFR(sol,geometry,Nf,L);

qa = sol(1:2,1);
pplat = sol(1+2+2*3*Nf:2+2*3*Nf+3,1);
[Rplat,~,~,~] = rotationParametrization(sol(1+2+2*3*Nf+3:2+2*3*Nf+6,1),rotparams);

PlotSpatialRFRFR(geometry,qa,posbeams,pplat,Rplat);
