% convert derivative of angles into twist (local)
function M = rot2twist(ang,str)
switch str
    case 'ZYZ'
        % R = Rz(a) Ry(b) Rz(g)
        % twist = M [a;b;g]' --> twist in global frame
        a = ang(1);
        b = ang(2);

        M = [0, -sin(a), +cos(a)*sin(b);
             0, +cos(a), +sin(a)*sin(b);
             1,  0,      +cos(b);];

    case 'XYZ'
        % R = Rx(a) Ry(b) Rz(g)
        % twist = M [a;b;g]' --> twist in global frame
        a = ang(1);
        b = ang(2);
        M = [1,0      , +sin(b);
             0,+cos(a), -cos(b)*sin(a);
             0,+sin(a), +cos(b)*cos(a);];
end