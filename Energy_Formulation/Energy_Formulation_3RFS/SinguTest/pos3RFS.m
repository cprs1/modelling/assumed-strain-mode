function posbeams = pos3RFS(guess,geometry,Nf,L)
Nsh = 100;
basepoints = geometry.basepoints;
baseangles = geometry.baseangles;
qa = guess(1:3,1);
qe = guess(1+3:3+3*3*Nf,1);

posbeams = zeros(3*3,Nsh);

for i = 1:3
    % extract variables
    p01 = basepoints(:,i);
    h01 = eul2quat([baseangles(i),0,qa(i)],'ZYX')';
    qe1 = qe(1+3*Nf*(i-1):i*3*Nf,1);
    % integrate
    y01 = [p01;h01];
    fun1 = @(s,y) OdeFunReconstruct(s,y,qe1,Nf,L);
    [s1,y1] = ode45(fun1,[0,1],y01);

    % spline results
    sspan1 = linspace(0,L,Nsh);
    posbeams(1+3*(i-1):3*i,:) = spline(L*s1,y1(:,1:3)',sspan1);
end

end