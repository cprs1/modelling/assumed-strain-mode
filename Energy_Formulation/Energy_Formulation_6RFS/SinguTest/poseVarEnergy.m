function [eq,grade,gradmu] = poseVarEnergy(h,mu,fplat,platformpoints,Nleg)

yaw = h(1);
pitch = h(2);
roll = h(3);

% platform external load contribution
dVextdp = -fplat;
dVextdh = zeros(3,1); %assume no external moment, only force

dCldh = zeros(3,1);
dCldp = -repmat(eye(3),1,6)*mu;

gradClpmu =  -repmat(eye(3),1,6);
gradClhmu = zeros(3,3*6);
for i = 1 : Nleg
    mui = mu(1+3*(i-1):3*i,1);
    dCldh = dCldh - [dRxdt(yaw)*Ry(pitch)*Rz(roll)*platformpoints(:,i),Rx(yaw)*dRydt(pitch)*Rz(roll)*platformpoints(:,i),dRxdt(yaw)*Ry(pitch)*dRzdt(roll)*platformpoints(:,i)]*mui;
    gradClhmu(:,1+3*(i-1):3*i) = - [dRxdt(yaw)*Ry(pitch)*Rz(roll)*platformpoints(:,i),Rx(yaw)*dRydt(pitch)*Rz(roll)*platformpoints(:,i),dRxdt(yaw)*Ry(pitch)*dRzdt(roll)*platformpoints(:,i)];
end

% collect
eq = [dVextdp+dCldp;dVextdh+dCldh];

% derivative of eq wrt qp
grade = zeros(6,6);

% derivative of eq wrt multipliers
gradmu = [gradClpmu;gradClhmu];
end