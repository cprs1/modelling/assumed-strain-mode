%% Inverse Geometrico Static Problem
% weak form solved with Assumed Mode - energetic approach
% 6-EFS Robot

% 15-Dec-2021 Federico Zaccaria

clear
close all
clc

%% Robot parameters
r = 0.001; % beams cross section radius [m]
I = 0.25*pi*r^4; % inertial moment of area [m^4]
J = 2*I;
E = 200*10^9; % young modulus [Pa]
EI = E*I; 
G = 80*10^9; % shear modulus [Pa]
Kbt = diag([EI;EI;G*J]);
L = 1; % beams lenght [m]
fend = [0;0;0]; % end-eff. force [N]
fd = [0;0;0]; % distributed force

rb = 0.3;
rp = 0.2;
db = 0.2;
dp = 0.05;

th1 =   0*pi/180;
th2 = 120*pi/180;
th3 = 240*pi/180;
th4 =  30*pi/180;
th5 = 150*pi/180;
th6 = 270*pi/180;

angles = [th1,th2,th3];

b1 = Rz(th1)*[0;rb;0]; % WRT base frame
b2 = Rz(th2)*[0;rb;0];
b3 = Rz(th3)*[0;rb;0];

b1A1 = +Rz(th1)*[db;0;0];
b1A2 = -Rz(th1)*[db;0;0];
b2A3 = +Rz(th2)*[db;0;0];
b2A4 = -Rz(th2)*[db;0;0];
b3A5 = +Rz(th3)*[db;0;0];
b3A6 = -Rz(th3)*[db;0;0];

A1 = b1 +b1A1;
A2 = b1 +b1A2;
A3 = b2 +b2A3;
A4 = b2 +b2A4;
A5 = b3 +b3A5;
A6 = b3 +b3A6;

basepoints = [A1,A2,A3,A4,A5,A6];

p1 = Rz(th4)*[rp;0;0]; % WRT platform frame
p2 = Rz(th5)*[rp;0;0];
p3 = Rz(th6)*[rp;0;0];

p1B1 = +Rz(th4)*[0;dp;0];
p1B2 = -Rz(th4)*[0;dp;0];
p2B3 = +Rz(th5)*[0;dp;0];
p2B4 = -Rz(th5)*[0;dp;0];
p3B5 = +Rz(th6)*[0;dp;0];
p3B6 = -Rz(th6)*[0;dp;0];

B1 = p1+p1B1;
B2 = p1+p1B2;
B3 = p2+p2B3;
B4 = p2+p2B4;
B5 = p3+p3B5;
B6 = p3+p3B6;

platformpoints = [B1,B4,B3,B6,B5,B2];

baseangles = [angles(1);angles(1);angles(2);angles(2);angles(3);angles(3)];

% reorient points

geometry.basepoints = basepoints;
geometry.platformpoints = platformpoints;
geometry.baseangles = baseangles;
geoparams = [A1,A2,A3,B1,B2,B3];

%% End Effector position
roll = 20*pi/180;
pitch = 20*pi/180;
yaw = 20*pi/180;

pplat = [0;0;0.8];

qpd = [pplat;roll;pitch;yaw];

%% Initial Guess
Nf = 4;
Nleg = 6;

hp = [roll;pitch;yaw];

qa0 = 0.7*ones(6,1);
A0 = zeros(3*Nf,1);
qe0 = repmat(A0,Nleg,1);
qp0 = [pplat;hp];
lambda0 = zeros(3*Nleg,1);
guess0 = [qa0;qe0;qp0;lambda0];

%% Solution
options = optimoptions('fsolve','display','iter-detailed','SpecifyObjectiveGradient',true,'CheckGradients',true,'Algorithm','trust-region');
Phi = @(s) PhiMatr(s,1,Nf);
K = integral( @(s) Phi(s)'*Kbt*Phi(s),0,1,'ArrayValued',true);
fun = @(y) AssumedModeIGSPeqn6EFS(y,Nleg,Nf,baseangles,basepoints,platformpoints,K,qpd,fd,fend);
[sol,~,flag,~,jac] = fsolve(fun,guess0,options);

%% Plot
Nftot = 3*Nf;
Nplot = 100;
qa = sol(1:Nleg,1); % actuated variables
qe = sol(Nleg+1:Nleg*(1+Nftot),1); % strains
qp = sol(Nleg*(1+Nftot)+1:Nleg*(1+Nftot)+6,1); % pose variables

s = linspace(0,L,Nplot);
posbeams = zeros(3*Nleg,Nplot);
for i = 1:Nleg
    Nftot = 3*Nf;
    Li = qa(i);
    qei = qe(1+Nftot*(i-1):Nftot*i,1);
    fun = @(s,y) OdefunAssumedMode(s,y,qei,fd,Nf,Li,'variable');
    Sspan = [0,Li];
    h0 = eul2quat([baseangles(i),0,0],'ZYX')';
    p0 = basepoints(:,i);
    Jh0 = zeros(4*Nftot,1);
    Jp0 = zeros(3*Nftot,1);
    Qw0 = zeros(Nftot,1);
    y01 = [h0;p0;Jh0;Jp0;Qw0];
    
    dhdqa0 = zeros(4,1);
    dpdqa0 = zeros(3,1);
    dJhdqa0 = zeros(4*Nftot,1);
    dJpdqa0 = zeros(3*Nftot,1);
    dQwdqa0 = zeros(Nftot,1);
    y02 = [dhdqa0;dpdqa0;dJhdqa0;dJpdqa0;dQwdqa0];
    
    dJhdqei0 = zeros(4*Nftot*Nftot,1);
    dJpdqei0 = zeros(3*Nftot*Nftot,1);
    dQwdqei0 = zeros(Nftot*Nftot,1);
    y03 = [dJhdqei0;dJpdqei0;dQwdqei0];
    
    y0 = [y01;y02;y03];

    [si,yi] = ode45(fun,Sspan,y0);
    pii = yi(:,5:7)';
    posbeams(1+3*(i-1):3*i,:) = spline(si,pii,s);

    nn = numel(yi(1,:));
end 
figure()
pplat = qp(1:3);
Rplat = Rz(qp(4))*Ry(qp(5))*Rz(qp(6));
Plot6EFS(geometry,posbeams,pplat,Rplat);