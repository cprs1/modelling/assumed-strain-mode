function flag = StabilityMode6EFR(jac,Nfm)
Nf = 3*Nfm;


U = jac(1:6*Nf+6,1+6:6+6*Nf);
P = jac(1:6*Nf+6,1+6+6*Nf:6+6*Nf+6);

G = jac(1:6*Nf+6,1+6+6*Nf+6:6+6+6*Nf+6*3);
Z = null(G');

H = [U,P];
Hr = Z'*H*Z;

[~,flag] = chol(Hr);

end