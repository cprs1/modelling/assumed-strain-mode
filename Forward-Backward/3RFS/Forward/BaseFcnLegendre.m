function b = BaseFcnLegendre(x,L,Nf)
s = x/L;
if Nf>6
    error('Maximum order 5, provide Legendre basis for N>6');
end

vect =   [ones(1,numel(s)) ;
         2.*s - 1 ;
         6.*s.^2 - 6.*s + 1; 
         20*s.^3 - 30*s.^2 + 12.*s - 1;
         70*s.^4 - 140*s.^3 + 90.*s.^2 - 20.*s + 1;
         252*s.^5 - 630*s.^4+560*s.^3-210*s.^2+30.*s-1;
         ];
% vect =   [ones(1,numel(s)) ;
%          s ;
%          s.^2 ; 
%          s.^3;
%          s.^4;
%          s.^5;
%          ];

b = vect(1:Nf,:);

end