function M = PhiMatr(x,L,Nf)
b = BaseFcnLegendre(x,L,Nf);
M = [b',zeros(1,Nf),zeros(1,Nf);
     zeros(1,Nf),b',zeros(1,Nf);
     zeros(1,Nf),zeros(1,Nf),b';];
end