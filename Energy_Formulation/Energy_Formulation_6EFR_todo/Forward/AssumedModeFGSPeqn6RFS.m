function [eq,gradeq] = AssumedModeFGSPeqn6RFS(y,Nleg,Nf,baseorients,basepoints,platformpoints,K,qad,f,fext,L)

Nftot = 3*Nf;
% extract variables
qa = y(1:6,1); % actuated variables
qe = y(1+6:1+6 + 6*Nftot,1); % strains
qp = y(1+6+6*Nftot:6+6*Nftot+6,1); % pose variables
mult = y(1+6+6*Nftot+6:6+6*Nftot+6+3*6,1); % closure loop multipliers

pp = qp(1:3,1);
hp = qp(4:6,1);
roll = hp(1);
pitch = hp(2);
yaw = hp(3);
Rp = Rx(roll)*Ry(pitch)*Rz(yaw);

% initialization
beameq = zeros(6*Nftot,1);
dbeameqdqa = zeros(6*Nftot,6);
dbeameqdqe = zeros(6*Nftot,6*Nftot);
dbeameqdqp = zeros(6*Nftot,6);
dbeameqdlamb = zeros(6*Nftot,3*6);

constr = zeros(3*6,1);
dconstrdqa = zeros(3*6,1);
dconstrdqe = zeros(3*6,6*Nftot);
dconstrdqp = zeros(3*6,6);

dconstrtermdh = zeros(3,3);

for i = 1:Nleg
   
    % assign variables
    qei = qe(1+Nftot*(i-1):Nftot*i,1);
    lambda = mult(1+3*(i-1):3*i,1);
    jointpos = pp+Rp*platformpoints(:,i);
    
    % integrate IVPs
    fun = @(s,y) OdefunAssumedMode(s,y,qei,f,Nf,L);
    Sspan = [0,1];
    h0 = eul2quat([baseorients(i),0,qa(i)],'ZYX')';
    p0 = basepoints(:,i);
    Jh0 = zeros(4*Nftot,1);
    Jp0 = zeros(3*Nftot,1);
    Qw0 = zeros(Nftot,1);
    y01 = [h0;p0;Jh0;Jp0;Qw0];
    
    dhdqa0 = DerivativeMotorQuaternion([baseorients(i),0,qa(i)]);
    dpdqa0 = zeros(3,1);
    dJhdqa0 = zeros(4*Nftot,1);
    dJpdqa0 = zeros(3*Nftot,1);
    dQwdqa0 = zeros(Nftot,1);
    y02 = [dhdqa0;dpdqa0;dJhdqa0;dJpdqa0;dQwdqa0];
    
    dJhdqei0 = zeros(4*Nftot*Nftot,1);
    dJpdqei0 = zeros(3*Nftot*Nftot,1);
    dQwdqei0 = zeros(Nftot*Nftot,1);
    y03 = [dJhdqei0;dJpdqei0;dQwdqei0];
    
    y0 = [y01;y02;y03];

    [~,yi] = ode45(fun,Sspan,y0);
     y = yi(end,:)';
    y1 = y(1:7+8*Nftot,1);
    y2 = y(1+7+8*Nftot:14+16*Nftot,1);
    y3 = y(1+14+16*Nftot:end,1);

    pii = y1(5:7,1);
    Jpi = reshape(y1(1+7+4*Nftot:7+4*Nftot+3*Nftot,1),3,Nftot);
    Qwi = y1(1+7+4*Nftot+3*Nftot:7+4*Nftot+3*Nftot+Nftot,1);
       
    dJpidqai = reshape(y2(1+7+4*Nftot:7+4*Nftot+3*Nftot,1),3,Nftot); 
    dQwidqai = y2(1+7+4*Nftot+3*Nftot:7+4*Nftot+3*Nftot+Nftot,1);
    dpidqai = y2(1+4:7,1);

    dJpidqei = reshape(y3(1+4*Nftot*Nftot:+4*Nftot*Nftot+3*Nftot*Nftot,1),3*Nftot,Nftot);
    dQwidqei = reshape(y3(1+4*Nftot*Nftot+3*Nftot*Nftot:+4*Nftot*Nftot+3*Nftot*Nftot+Nftot*Nftot,1),Nftot,Nftot);
    
    %% BEAM EQUATIONS
    beameq(1+Nftot*(i-1):Nftot*i,1) = L*K*qei + Jpi'*lambda - Qwi;
    dbeameqdqa(1+Nftot*(i-1):Nftot*i,i) = dJpidqai'*lambda - dQwidqai;
    constr_matr = zeros(Nftot,Nftot);
    for j = 1:Nftot
        constr_matr(j,:) = lambda'*dJpidqei(1+3*(j-1):3*j,:);
    end
    dbeameqdqe(1+Nftot*(i-1):Nftot*i,1+Nftot*(i-1):Nftot*i) = K + constr_matr - dQwidqei;
    dbeameqdlamb(1+Nftot*(i-1):Nftot*i,1+3*(i-1):3*i) = Jpi';
    
    %% CLOSURE EQUATIONS
    constr(1+3*(i-1):3*i,1) = pii-jointpos;
    dconstrdqa(1+3*(i-1):3*i,i) = dpidqai; 
    dconstrdqe(1+3*(i-1):3*i,1+Nftot*(i-1):Nftot*i) = Jpi;
    djointposdh = [dRxdt(roll)*Ry(pitch)*Rz(yaw)*platformpoints(:,i),Rx(roll)*dRydt(pitch)*Rz(yaw)*platformpoints(:,i),Rx(roll)*Ry(pitch)*dRzdt(yaw)*platformpoints(:,i)];
    dconstrdqp(1+3*(i-1):3*i,:) = -[eye(3),djointposdh];
    
    % term usefull for platform eq
    d1  = [lambda'*dRxdt(roll+pi/2)*Ry(pitch)*Rz(yaw)*platformpoints(:,i),lambda'*dRxdt(roll)*dRydt(pitch)*Rz(yaw)*platformpoints(:,i),lambda'*dRxdt(roll)*Ry(pitch)*dRzdt(yaw)*platformpoints(:,i)];
    d2 = [lambda'*dRxdt(roll)*dRydt(pitch)*Rz(yaw)*platformpoints(:,i),lambda'*Rx(roll)*dRydt(pitch+pi/2)*Rz(yaw)*platformpoints(:,i),lambda'*Rx(roll)*dRydt(pitch)*dRzdt(yaw)*platformpoints(:,i)];
    d3   = [lambda'*dRxdt(roll)*Ry(pitch)*dRzdt(yaw)*platformpoints(:,i),lambda'*Rx(roll)*dRydt(pitch)*dRzdt(yaw)*platformpoints(:,i),lambda'*Rx(roll)*Ry(pitch)*dRzdt(yaw+pi/2)*platformpoints(:,i)];

    contribution = [d1;d2;d3];
    dconstrtermdh = dconstrtermdh - contribution;
end

%% PLATFORM EQUATIONS
plateq = -[fext;zeros(3,1)] + dconstrdqp'*mult; 
dplateqdlamb = dconstrdqp';
dplateqdqp = [zeros(3,6);zeros(3,3),dconstrtermdh];

%% FORWARD PROBLEM EQUATIONS
forw = qa-qad;

%% ASSEMBLY
eq = [beameq;plateq;constr;forw];

gradeq = [dbeameqdqa, dbeameqdqe,        dbeameqdqp, dbeameqdlamb;
          zeros(6,6), zeros(6,6*Nftot),  dplateqdqp, dplateqdlamb;
          dconstrdqa, dconstrdqe,        dconstrdqp, zeros(3*6,3*6);
          eye(6,6),   zeros(6,6*Nftot),  zeros(6,6), zeros(6,6*3);];
end