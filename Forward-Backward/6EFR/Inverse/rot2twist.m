% convert derivative of angles into twist (local)
function M = rot2twist(ang,str)
switch str
    case 'ZYZ'
        % R = Rz(a) Ry(b) Rz(g)
        % twist = M [a;b;g]' --> twist in global frame
        a = ang(1);
        b = ang(2);
        M = [0,-sin(a), +sin(b)*cos(a);
             0,+cos(a), +sin(b)*sin(a);
             1,0      , +cos(b);];
end
end