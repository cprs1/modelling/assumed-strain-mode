function [eq,gradeq] = ForwardMode3RFR(guess,geometry,L,Kee,qad,wp,wd,Nf)

basepoints = geometry.basepoints;
platpoints = geometry.platpoints;

qa = guess(1:3,1);
qe = guess(1+3:3+3*Nf,1);
qp = guess(1+3+3*Nf:3+3*Nf+3,1);
lambda = guess(1+3+3*Nf+3:3+3*Nf+3+6,1);

pplat = qp(1:2,1);
thplat = qp(3,1);
Rp = Rz(thplat);
dRpdthp = Rz(thplat+pi/2);

Ci = [zeros(1,2);eye(2)];

% matrix initialization
beameq = zeros(3*Nf,1);
dbeamdqa = zeros(3*Nf,3);
dbeamdqe = zeros(3*Nf,3*Nf);
dbeamdlambd = zeros(3*Nf,6);
geomconstr = zeros(6,1);
dconsdqa = zeros(6,3);
dconsdqe = zeros(6,3*Nf);
dconsdqp = zeros(6,3);
force_eq = wp(2:3);
momen_eq = wp(1);
dforcedqa = zeros(2,3);
dforcedqe = zeros(2,3*Nf);
dforcedqp = zeros(2,3);
dforcedlambd = zeros(2,6);
dmomdqa = zeros(1,3);
dmomdqe = zeros(1,3*Nf);
dmomdqp = zeros(1,3);
dmomdlambd = zeros(1,6);

for k = 1:3
    % BEAM  INTEGRATION
    % extract variables
    p0 = basepoints(:,k);
    pp = Rp*platpoints(:,k);
    pLp = pplat +pp;
    th0 = qa(k,1);
    qei = qe(1+Nf*(k-1):k*Nf,1);
    lambdai = lambda(1+2*(k-1):2*k,1);
    wrench = (Ci*lambdai);
    % integrate forward
    y0F = [p0;th0;zeros(2+2*Nf,1);1;zeros(Nf,1)];
    fun = @(s,y) OdefunAssumedForward(s,y,qei,Nf,L,'fix');
    [~,y] = ode45(fun,[0,1],y0F);
    ygeom = y(end,:)';
    
    % extract results
    pL = ygeom(1:2,1);
    thL = ygeom(3,1);
    dpxdqeL = ygeom(1+5:5+Nf,1)';
    dpydqeL = ygeom(1+5+Nf:5+2*Nf,1)';
    dpLdqa = ygeom(4:5,1);
    dpLdqe = [dpxdqeL;dpydqeL];
    dthLdqa = ygeom(1+5+2*Nf,1);
    dthLdqe = ygeom(1+5+2*Nf+1:end,1)';
    
    % integrate backward
    y0B = [wrench;zeros(Nf,1);zeros(3+Nf,1);zeros(3*Nf+Nf*Nf,1);reshape(eye(3),3*3,1);zeros(3*Nf,1)];
    
    % integration
    funbackward = @(s,y) OdefunAssumeBackward(s,y,qei,wd,Nf,L,'fix');
    [~,y] = ode45(funbackward,[1,0],y0B);
    yforces = y(end,:)';
    
    % extract results
    Qc =   yforces(1+3:3+Nf,1);
    dQcdqa = yforces(1+3+Nf+3:3+Nf+3+Nf,1);
    dQcdqe = reshape(yforces(1+3+Nf+3+Nf+3*Nf:3+Nf+3+Nf+3*Nf+Nf*Nf,1),Nf,Nf);
    dQcdw0 = reshape(yforces(1+3+Nf+3+Nf+3*Nf+Nf*Nf+3*3:3+Nf+3+Nf+3*Nf+Nf*Nf+3*3+3*Nf,1),Nf,3);
    
    % platform equibribrium contributions
    Rtip = Rz(thL);
    dRtipdth = Rz(thL+pi/2);
    
    n = -Rtip*wrench(2:3);
    m = -wrench(1) +n(2)*pp(1,1) -n(1)*pp(2,1);
    
    dndqa = -dRtipdth*wrench(2:3)*dthLdqa;
    dndqe = -dRtipdth*wrench(2:3)*dthLdqe;
    dndlambd = -Rtip;
    dmdqa = +dndqa(2)*pp(1,1) -dndqa(1)*pp(2,1);
    dmdqe = +pp(1,1)*dndqe(2,:) -pp(2,1)*dndqe(1,:);
    dppdthp = dRpdthp*platpoints(:,k);
    dmdthp = +n(2)*dppdthp(1,1) -n(1)*dppdthp(2,1);
    dmdlambd = +pp(1,1)*dndlambd(2,:) -pp(2,1)*dndlambd(1,:);
    
    % equations and gradient
    beameq(1+Nf*(k-1):Nf*k,1) = L*Kee*qei + Qc;
    dbeamdqa(1+Nf*(k-1):Nf*k,k) = dQcdqa;
    dbeamdqe(1+Nf*(k-1):Nf*k,1+Nf*(k-1):Nf*k) = L*Kee + dQcdqe;
    dbeamdlambd(1+Nf*(k-1):Nf*k,1+2*(k-1):2*k) = dQcdw0*Ci;
    geomconstr(1+2*(k-1):2*k,1)  = pL-pLp;
    dconsdqa(1+2*(k-1):2*k,k) = dpLdqa;
    dconsdqe(1+2*(k-1):2*k,1+Nf*(k-1):Nf*k) = dpLdqe;
    dconsdqp(1+2*(k-1):2*k,:) = -[eye(2),dppdthp];
    force_eq = force_eq + n;
    momen_eq = momen_eq + m;
    dforcedqa(:,k) = dndqa;
    dforcedqe(:,1+Nf*(k-1):Nf*k) = dndqe;
    dforcedlambd(:,1+2*(k-1):2*k) = dndlambd;
    dmomdqa(:,k) = dmdqa;
    dmomdqe(:,1+Nf*(k-1):Nf*k) = dmdqe;
    dmomdqp = dmomdqp + [zeros(1,2), dmdthp];
    dmomdlambd(:,1+2*(k-1):2*k) = dmdlambd;

end

equilibrium = [force_eq;momen_eq];
dequidqa = [dforcedqa;dmomdqa];
dequidqe = [dforcedqe;dmomdqe];
dequidqp = [dforcedqp;dmomdqp];
dequidlambd = [dforcedlambd;dmomdlambd];

% inverse problem

forw = qa-qad;

% collect

eq = [beameq;equilibrium;geomconstr;forw];

gradeq = [dbeamdqa,   dbeamdqe,      zeros(3*Nf,3), dbeamdlambd;
          dequidqa,   dequidqe,      dequidqp,      dequidlambd;
          dconsdqa,   dconsdqe,      dconsdqp,      zeros(6,6);
          eye(3,3),   zeros(3,3*Nf), zeros(3,3),      zeros(3,6);];
end