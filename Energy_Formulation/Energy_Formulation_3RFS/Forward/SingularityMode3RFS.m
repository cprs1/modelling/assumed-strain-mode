function [flag1,flag2] = SingularityMode3RFS(jac,Nfm)

Nf = 3*Nfm;

A1 = jac(1:3*Nf+6,1:3);
U1_el = jac(1:3*Nf+6,1+3:3+3*Nf);
P1_full = jac(1:3*Nf+6,1+3+3*Nf:3+3*Nf+6);
P1 = P1_full(:,1:3);
P1_orient = P1_full(:,4:6);
U1 = [U1_el,P1_orient];

A2 = jac(1+3*Nf+6:3*Nf+6+3*3,1:3);
U2_el = jac(1+3*Nf+6:3*Nf+6+3*3,1+3:3+3*Nf);
P2_full = jac(1+3*Nf+6:3*Nf+6+3*3,1+3+3*Nf:3+3*Nf+6);
P2 = P2_full(:,1:3);
P2_orient = P2_full(:,4:6);
U2 = [U2_el,P2_orient];

G = jac(1:3*Nf+6,1+3+3*Nf+6:3+6+3*Nf+3*3);
Z = null(G');

T1 = [Z'*A1,Z'*U1;
         A2,   U2];
     
T2 = [Z'*P1,Z'*U1; 
         P2,   U2];

flag1 = rcond(T1);
flag2 = rcond(T2);

end