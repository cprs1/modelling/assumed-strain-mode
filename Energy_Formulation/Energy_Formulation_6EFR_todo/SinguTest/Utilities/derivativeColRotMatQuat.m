% DERIVATIVE OF ROTATION MATRIX COLUMNS WRT QUATERNION
% hL = h1 + h2 i + h3 j + h4 k (h1 = scalar component)
%  D1 = derivative of R(:,1) wrt hL
%  D2 = derivative of R(:,2) wrt hL
%  D3 = derivative of R(:,3) wrt hL
function [D1,D2,D3] = derivativeColRotMatQuat(hL)
    D1 = 2*[+hL(1,1),+hL(2,1),-hL(3,1),-hL(4,1);-hL(4,1),+hL(3,1),+hL(2,1),-hL(1,1);+hL(3,1),+hL(4,1),+hL(1,1),+hL(2,1)];
    D2 = 2*[+hL(4,1),+hL(3,1),+hL(2,1),+hL(1,1);+hL(1,1),-hL(2,1),+hL(3,1),-hL(4,1);-hL(2,1),-hL(1,1),+hL(4,1),+hL(3,1)];
    D3 = 2*[-hL(3,1),+hL(4,1),-hL(1,1),+hL(2,1);+hL(2,1),+hL(1,1),+hL(4,1),+hL(3,1);+hL(1,1),-hL(2,1),-hL(3,1),+hL(4,1)];
end