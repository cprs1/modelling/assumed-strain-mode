function [eq,gradeq] = InverseMode6EFR_1c(guess,geometry,Kee,qpd,wp,wd,Nf,Lp)
Nftot = 3*Nf;

basepoints = geometry.basepoints;
platpoints = geometry.platpoints;
diskpoints = geometry.diskpoints;
baseangles = geometry.baseangles;
rotparam = geometry.rotparam;

qa = guess(1:6,1);
qe1 = guess(1+6:6+6*Nftot,1);
qe2 = guess(1+6+6*Nftot:6+2*6*Nftot,1);
qepassive = guess(1+6+2*6*Nftot: 6+ 2*6*Nftot + Nftot,1);
Lfirst = guess(1+6+2*6*Nftot + Nftot:6+2*6*Nftot + Nftot+6,1);

qd = guess(1+6+2*6*Nftot + Nftot+6:6+2*6*Nftot + Nftot+6+6,1);
pdisk = qd(1:3,1);
[Rd,dRddrolld,dRddpitchd,dRddyawd] = rotationParametrization(qd(4:6,1),rotparam);


qp = guess(1+6+2*6*Nftot + Nftot+6+6:6+2*6*Nftot + Nftot+6+6+6);
pplat = qp(1:3,1);
[Rp,dRpdrollp,dRpdpitchp,dRpdyawp] = rotationParametrization(qp(4:6,1),rotparam);

lambdap = guess(1+6+2*6*Nftot+Nftot+6+6+6:6+2*6*Nftot+Nftot+6+6+6+5*6,1);
lambdad = guess(1+6+2*6*Nftot+Nftot+6+6+6+5*6:6+2*6*Nftot+Nftot+6+6+6+5*6+4*6,1);
lambdapassive = guess(1+6+2*6*Nftot+Nftot+6+6+6+5*6+4*6:6+2*6*Nftot+Nftot+6+6+6+5*6+4*6+5,1);

C2 = [eye(2),zeros(2,3);zeros(1,5);zeros(3,2),eye(3)];
C1 = [eye(2),zeros(2);zeros(1,4);zeros(2),eye(2);zeros(1,4)];

% matrix initialization
beameq1 = zeros(6*Nftot,1);
beameq2 = zeros(6*Nftot,1);
dbeameq1dqa = zeros(6*Nftot,6);
dbeameq2dqa = zeros(6*Nftot,6);
dbeameq1dqe1 = zeros(6*Nftot,6*Nftot);
dbeameq1dqe2 = zeros(6*Nftot,6*Nftot);
dbeameq2dqe1 = zeros(6*Nftot,6*Nftot);
dbeameq2dqe2 = zeros(6*Nftot,6*Nftot);
dbeameq1dLf = zeros(6*Nftot,6);
dbeameq2dLf = zeros(6*Nftot,6);
dbeameq1dlamp = zeros(6*Nftot,5*6);
dbeameq1dlamd = zeros(6*Nftot,4*6);
dbeameq2dlamp = zeros(6*Nftot,5*6);
dbeameq2dlamd = zeros(6*Nftot,4*6);

geomconstrp = zeros(5*6,1);
geompdqa = zeros(5*6,6);
geompdqe1 = zeros(5*6,6*Nftot);
geompdqe2 = zeros(5*6,6*Nftot);
geompdLf = zeros(5*6,6);
geompdqp = zeros(5*6,6);
    
geomconstrd = zeros(4*6,1);
geomddqa = zeros(4*6,6);
geomddqe1 = zeros(4*6,6*Nftot);
geomddqe2 = zeros(4*6,6*Nftot);
geomddLf = zeros(4*6,6);
geomddqd = zeros(4*6,6);

elconstr = zeros(6,1);
elconstrdqa = zeros(6,6);
elconstrdqe1 = zeros(6,6*Nftot);
elconstrdqe2 = zeros(6,6*Nftot);
elconstrdLf = zeros(6,6);
elconstrdqd = zeros(6,6);
    
wrencheqp = wp; % platform equilibrium
wqpdqa = zeros(6,6);
wqpdqe1 = zeros(6,6*Nftot);
wqpdqe2 = zeros(6,6*Nftot);
wqpdLf = zeros(6,6);
wqpdlamp = zeros(6,5*6);
wqpdqp = zeros(6,6);

wrencheqd = zeros(6,1); % disk equilibrium
wqddqa = zeros(6,1);
wqddqe1 = zeros(6,6*Nftot);
wqddqe2 = zeros(6,6*Nftot);
wqddLf = zeros(6,6);
wqddlamp = zeros(6,5*6);
wqddlamd = zeros(6,4*6);
wqddqd = zeros(6,6);

%% Beam integrations
for k = 1:6
    
    % extract variables
    L1 = Lfirst(k,1);
    L2 = qa(k)-L1;
    p0 = basepoints(:,k);
    h0 = eul2quat([baseangles(k),0,0],'ZYX')';
    
    pp1 = Rd*diskpoints(:,k);
    posdisk = pdisk +pp1; 
    dposdiskdrot = [dRddrolld*diskpoints(:,k),dRddpitchd*diskpoints(:,k),dRddyawd*diskpoints(:,k)];

    pp2 = Rp*platpoints(:,k);
    posplat = pplat +pp2;
    dposplatdrot = [dRpdrollp*platpoints(:,k),dRpdpitchp*platpoints(:,k),dRpdyawp*platpoints(:,k)];
    
    q1 = qe1(1+Nftot*(k-1):k*Nftot,1);
    q2 = qe2(1+Nftot*(k-1):k*Nftot,1);
    
    %% FORWARD INTEGRATIONs
    
    % integrate forward 1
    y0F1 = [p0;h0;zeros(3+3*Nftot,1);zeros(4+4*Nftot,1)];
    funforward1 = @(s,y) OdefunAssumedForward(s,y,q1,Nf,L1,'variable');
    [~,yf1] = ode45(funforward1,[0,1],y0F1);
    ygeom1 = yf1(end,:)';
    
    % extract results 1
    pL1 = ygeom1(1:3,1);
    hL1 = ygeom1(4:7,1);
    Rtip1 = quat2rotmatrix(hL1);
    dpL1dqa = zeros(3,1);
    dhL1dqa = zeros(4,1);
    dpL1dq1 = reshape(ygeom1(1+7+3:7+3+3*Nftot,1),3,Nftot);
    dhL1dq1 = reshape(ygeom1(1+7+3+3*Nftot+4:7+3+3*Nftot+4+4*Nftot,1),4,Nftot);
    dpL1dLf = ygeom1(1+7:7+3,1);
    dhL1dLf = ygeom1(1+7+3+3*Nftot:7+3+3*Nftot+4,1);
    dpL1dq2 = zeros(3,Nftot);
    dhL1dq2 = zeros(4,Nftot);
    
    % integrate forward 2
     
    y0F2 = [pL1;hL1;zeros(3+3*Nftot,1);zeros(4+4*Nftot,1)];
    funforward2 = @(s,y) OdefunAssumedForward(s,y,q2,Nf,L2,'variable');
    [~,yf2] = ode45(funforward2,[0,1],y0F2);
    ygeom2 = yf2(end,:)';
    
    % extract results 2
    pL2 = ygeom2(1:3,1);
    hL2 = ygeom2(4:7,1);
    Rtip2 = quat2rotmatrix(hL2);
    
    y0s = [pL1;hL1;reshape(eye(3),3*3,1);zeros(4*3*2,1);reshape(eye(4),4*4,1)]; 
    funsens = @(s,y) initvalueSensitivity(s,y,L2,q2,Nf);
    [~,ys] = ode45(funsens,[0,1],y0s);
    yss = ys(end,:)';
    dpL2dpL1 = reshape( yss(1+3+4             :3+4+3*3,1),3,3);
    dhL2dpL1 = reshape( yss(1+3+4+3*3         :3+4+3*3+4*3,1),4,3);
    dpL2dhL1 = reshape( yss(1+3+4+3*3+4*3     :3+4+3*3+4*3+3*4,1),3,4);
    dhL2dhL1 = reshape( yss(1+3+4+3*3+4*3+3*4 :3+4+3*3+4*3+3*4+4*4,1),4,4);

    dpL2dqa = +ygeom2(1+7:7+3,1);
    dhL2dqa = +ygeom2(1+7+3+3*Nftot:7+3+3*Nftot+4,1);
    dpL2dq1 = dpL2dpL1*dpL1dq1 + dpL2dhL1*dhL1dq1;
    dhL2dq1 = dhL2dhL1*dhL1dq1 + dhL2dpL1*dpL1dq1;
    dpL2dq2 = reshape(ygeom2(1+7+3:7+3+3*Nftot,1),3,Nftot);
    dhL2dq2 = reshape(ygeom2(1+7+3+3*Nftot+4:7+3+3*Nftot+4+4*Nftot,1),4,Nftot);
    dpL2dLf = - ygeom2(1+7:7+3,1) + dpL2dpL1*dpL1dLf + dpL2dhL1*dhL1dLf; 
    dhL2dLf = - ygeom2(1+7+3+3*Nftot:7+3+3*Nftot+4,1) + dhL2dpL1*dpL1dLf + dhL2dhL1*dhL1dLf;
    
    %% BACKWARD INTEGRATIONs 
    % extract variables
    lambda2 = lambdap(1+5*(k-1):5*k,1); % mx my fx fy fz in local tip frame
    lambda1 = lambdad(1+4*(k-1):4*k,1); % mx my fx fy in local tip frame
    % integration 2
    wrench2 = (C2*lambda2);
    y0B2 = [wrench2;zeros(Nftot,1);zeros(6+Nftot,1);zeros(Nftot*(6+Nftot),1);reshape(eye(6),6*6,1);zeros(6*Nftot,1)];

    funbackward2 = @(s,y) OdefunAssumeBackward(s,y,q2,wd,Nf,L2,'variable');
    [~,yb2] = ode45(funbackward2,[1,0],y0B2);
    yforces2 = yb2(end,:)';
    
    % extract results 2
    Fc2 = yforces2(1:6,1);
    Qc2 = yforces2(1+6:6+Nftot,1);
    dFc2dqa = + yforces2(1+6+Nftot:+6+Nftot+6,1);
    dFc2dLf = - yforces2(1+6+Nftot:+6+Nftot+6,1);
    dQc2dqa = + yforces2(1+6+Nftot+6:6+Nftot+6+Nftot,1);
    dFc2dq2 = reshape(yforces2(1+6+Nftot+6+Nftot:6+Nftot+6+Nftot+6*Nftot,1),6,Nftot);
    dQc2dq2 = reshape(yforces2(1+6+Nftot+6+Nftot+6*Nftot:6+Nftot+6+Nftot+6*Nftot+Nftot*Nftot,1),Nftot,Nftot);
    dQc2dq1 = zeros(Nftot,Nftot);
    dQc2dw02 = reshape(yforces2(1+6+Nftot+6+Nftot+Nftot*(6+Nftot)+6*6:6+Nftot+6+Nftot+Nftot*(6+Nftot)+6*(6+Nftot)),Nftot,6);
    dFc2dw02 = reshape(yforces2(1+6+Nftot+6+Nftot+Nftot*(6+Nftot):6+Nftot+6+Nftot+Nftot*(6+Nftot)+6*6),6,6);
    dQc2dlamp = dQc2dw02*C2;
    dQc2dLf = - yforces2(1+6+Nftot+6:6+Nftot+6+Nftot,1);
    
    % integration 1
    wrench1 = [lambda1(1:2);Fc2(3);lambda1(3:4);Fc2(6)]; % since the frame are the same, not necessity to rotate
    y0B1 = [wrench1;zeros(Nftot,1);zeros(6+Nftot,1);zeros(Nftot*(6+Nftot),1);reshape(eye(6),6*6,1);zeros(6*Nftot,1)];

    funbackward1 = @(s,y) OdefunAssumeBackward(s,y,q1,wd,Nf,L1,'variable');
    [~,yb1] = ode45(funbackward1,[1,0],y0B1);
    yforces1 = yb1(end,:)';
    
    dw01dFc2 = [zeros(2,6);zeros(1,2),1,zeros(1,3);zeros(2,6);zeros(1,5),1];
    
    Qc1 = yforces1(1+6:6+Nftot,1);
    dQc1dq1 = reshape(yforces1(1+6+Nftot+6+Nftot+6*Nftot:6+Nftot+6+Nftot+6*Nftot+Nftot*Nftot,1),Nftot,Nftot);
    dQc1dw01 = reshape(yforces1(1+6+Nftot+6+Nftot+Nftot*(6+Nftot)+6*6:6+Nftot+6+Nftot+Nftot*(6+Nftot)+6*(6+Nftot)),Nftot,6);
    dQc1dqa = dQc1dw01*dw01dFc2*dFc2dqa;
    dQc1dq2 = dQc1dw01*dw01dFc2*dFc2dq2;
    dQc1dLf = yforces1(1+6+Nftot+6:6+Nftot+6+Nftot,1) + dQc1dw01*dw01dFc2*dFc2dLf;
    dQc1dlamp = dQc1dw01*dw01dFc2*dFc2dw02*C2;
    dQc1dlamd = dQc1dw01*C1;
    
    %% EQUATIONS
    
    % platform equilibrium
    loc2glob2 = [Rtip2,zeros(3);zeros(3),Rtip2]; % rotate wrench in global frame
    gwrench2 = loc2glob2*(-wrench2); 
    [D1,D2,D3] = derivativeColRotMatQuat(hL2);
    matr2 = [wrench2(1:3,1)'*D1;wrench2(1:3,1)'*D2;wrench2(1:3,1)'*D3;wrench2(4:6,1)'*D1;wrench2(4:6,1)'*D2;wrench2(4:6,1)'*D3;];
    dgwrench2dqa = -matr2*dhL2dqa;
    dgwrench2dLf = -matr2*dhL2dLf;
    dgwrench2dq1 = -matr2*dhL2dq1;
    dgwrench2dq2 = -matr2*dhL2dq2;
    transl2 = [eye(3),skew(pp2);zeros(3),eye(3)]; % translate wrench to platform origin
    wrencheqp = wrencheqp + transl2*gwrench2; % equilibrium in platform frame 
    wqpdqa(:,k) = transl2*dgwrench2dqa;
    wqpdqe1(:,1+Nftot*(k-1):Nftot*k) = transl2*dgwrench2dq1;
    wqpdqe2(:,1+Nftot*(k-1):Nftot*k) = transl2*dgwrench2dq2;
    wqpdLf(:,k) = transl2*dgwrench2dLf;
    wqpdlamp(:,1+5*(k-1):5*k) = -transl2*loc2glob2*C2;
    mat2 = [skew(dposplatdrot(:,1))*gwrench2(4:6,1),skew(dposplatdrot(:,2))*gwrench2(4:6,1),skew(dposplatdrot(:,3))*gwrench2(4:6,1)];
    wqpdqp = wqpdqp + +[zeros(3,3),mat2;zeros(3,6)];  
    
    % disk equilibrium
    loc2glob1 = [Rtip1,zeros(3);zeros(3),Rtip1]; % rotate wrench in global frame
    wrench1p = C1*lambda1;
    gwrench1 = loc2glob1*(-wrench1p);
    [D1,D2,D3] = derivativeColRotMatQuat(hL1);
    matr1 = [wrench1p(1:3,1)'*D1;wrench1p(1:3,1)'*D2;wrench1p(1:3,1)'*D3;wrench1p(4:6,1)'*D1;wrench1p(4:6,1)'*D2;wrench1p(4:6,1)'*D3;];
    dgwrench1pdqa = -matr1*dhL1dqa;
    dgwrench1pdLf = -matr1*dhL1dLf;
    dgwrench1pdq1 = -matr1*dhL1dq1;
    dgwrench1pdq2 = -matr1*dhL1dq2;
    transl1 = [eye(3),skew(pp1);zeros(3),eye(3)]; % translate wrench to platform origin
    wrencheqd = wrencheqd + transl1*gwrench1; % equilibrium in platform frame 
    wqddqa(:,k) = transl1*dgwrench1pdqa;
    wqddqe1(:,1+Nftot*(k-1):Nftot*k) = transl1*dgwrench1pdq1;
    wqddqe2(:,1+Nftot*(k-1):Nftot*k) = transl1*dgwrench1pdq2;
    wqddLf(:,k) = transl1*dgwrench1pdLf;
    wqddlamp(:,1+5*(k-1):5*k) = zeros(6,5); % TO VERIFY
    wqddlamd(:,1+4*(k-1):4*k) = -transl1*loc2glob1*C1;
    mat1 = [skew(dposdiskdrot(:,1))*gwrench1(4:6,1),skew(dposdiskdrot(:,2))*gwrench1(4:6,1),skew(dposdiskdrot(:,3))*gwrench1(4:6,1)];
    wqddqd = wqddqd + [zeros(3,3),mat1;zeros(3,6)];  
    
    % beams equilibrium
    beameq1(1+Nftot*(k-1):Nftot*k,1) = L1*Kee*q1 + Qc1; % first beam part equilibrium
    beameq2(1+Nftot*(k-1):Nftot*k,1) = L2*Kee*q2 + Qc2; % second beam part equilibrium
    dbeameq1dqa(1+Nftot*(k-1):Nftot*k,k) = dQc1dqa; % TO VERIFY
    dbeameq2dqa(1+Nftot*(k-1):Nftot*k,k) = Kee*q2 + dQc2dqa;
    dbeameq1dqe1(1+Nftot*(k-1):Nftot*k,1+Nftot*(k-1):Nftot*k) = L1*Kee + dQc1dq1;
    dbeameq1dqe2(1+Nftot*(k-1):Nftot*k,1+Nftot*(k-1):Nftot*k) = dQc1dq2;
    dbeameq2dqe1(1+Nftot*(k-1):Nftot*k,1+Nftot*(k-1):Nftot*k) = dQc2dq1;
    dbeameq2dqe2(1+Nftot*(k-1):Nftot*k,1+Nftot*(k-1):Nftot*k) = L2*Kee + dQc2dq2;
    dbeameq1dLf(1+Nftot*(k-1):Nftot*k,k) = +Kee*q1 + dQc1dLf;
    dbeameq2dLf(1+Nftot*(k-1):Nftot*k,k) = -Kee*q2 + dQc2dLf;
    dbeameq1dlamp(1+Nftot*(k-1):Nftot*k,1+5*(k-1):5*k) = dQc1dlamp;
    dbeameq1dlamd(1+Nftot*(k-1):Nftot*k,1+4*(k-1):4*k) = dQc1dlamd;
    dbeameq2dlamp(1+Nftot*(k-1):Nftot*k,1+5*(k-1):5*k) = dQc2dlamp;
    dbeameq2dlamd(1+Nftot*(k-1):Nftot*k,1+4*(k-1):4*k) = zeros(Nftot,4);
    
    % geometric constraints
    [angerr2,dangerr2dqa,dangerr2dq2] = roterr(Rp,Rtip2,hL2,dhL2dqa,dhL2dq2);
    [~,dangerr2dLf,dangerr2dq1] = roterr(Rp,Rtip2,hL2,dhL2dLf,dhL2dq1);
    dangerrdangp = [invskew(dRpdrollp'*Rtip2-Rtip2'*dRpdrollp),invskew(dRpdpitchp'*Rtip2-Rtip2'*dRpdpitchp),invskew(dRpdyawp'*Rtip2-Rtip2'*dRpdyawp)];
    geomconstrp(1+5*(k-1):5*k,1)  = [angerr2(1:2,1);pL2-posplat;]; % platform constraints
    geompdqa(1+5*(k-1):5*k,k) = [dangerr2dqa(1:2,1);dpL2dqa];
    geompdqe1(1+5*(k-1):5*k,1+Nftot*(k-1):Nftot*k) = [dangerr2dq1(1:2,:);dpL2dq1];
    geompdqe2(1+5*(k-1):5*k,1+Nftot*(k-1):Nftot*k) = [dangerr2dq2(1:2,:);dpL2dq2];
    geompdLf(1+5*(k-1):5*k,k) = [dangerr2dLf(1:2,1);dpL2dLf];
    geompdqp(1+5*(k-1):5*k,:) = [ zeros(2,3),-dangerrdangp(1:2,:);-eye(3),-dposplatdrot;]; 
    
    disk_loc_pos = Rd'*(pL1-posdisk); % Rd'*(pL1-pdisk+Rd*diskpoints(:,k);) = Rd'*(pL1-pdisk) + diskpoints(:,k);
    ddisk_loc_posdqa = Rd'*(dpL1dqa);
    ddisk_loc_posdq1 = Rd'*(dpL1dq1);
    ddisk_loc_posdq2 = Rd'*(dpL1dq2);
    ddisk_loc_posdLf = Rd'*(dpL1dLf);
    [angerr1,dangerr1dLf,dangerr1dq1] = roterr(Rd,Rtip1,hL1,dhL1dLf,dhL1dq1);
    [~,dangerr1dqa,dangerr1dq2] = roterr(Rd,Rtip1,hL1,dhL1dqa,dhL1dq2);
    dangerrdangd = [invskew(dRddrolld'*Rtip1-Rtip1'*dRddrolld),invskew(dRddpitchd'*Rtip1-Rtip1'*dRddpitchd),invskew(dRddyawd'*Rtip1-Rtip1'*dRddyawd)];
    geomconstrd(1+4*(k-1):4*k,1)  = [angerr1(1:2,1);disk_loc_pos(1:2)]; % platform constraints
    geomddqa(1+4*(k-1):4*k,k) = [dangerr1dqa(1:2,1);ddisk_loc_posdqa(1:2)];
    geomddqe1(1+4*(k-1):4*k,1+Nftot*(k-1):Nftot*k) = [dangerr1dq1(1:2,:);ddisk_loc_posdq1(1:2,:)];
    geomddqe2(1+4*(k-1):4*k,1+Nftot*(k-1):Nftot*k) = [dangerr1dq2(1:2,:);ddisk_loc_posdq2(1:2,:)];
    geomddLf(1+4*(k-1):4*k,k) = [dangerr1dLf(1:2,1);ddisk_loc_posdLf(1:2)];
    dgdord = [dRddrolld'*(pL1-pdisk),dRddpitchd'*(pL1-pdisk),dRddyawd'*(pL1-pdisk)];    
    dgeomposddqd = [-Rd',dgdord];
    geomddqd(1+4*(k-1):4*k,:) =  [ zeros(2,3),-dangerrdangd(1:2,:);dgeomposddqd(1:2,:);];    
    
    % elastic constraint --> the disk imposes only local x-y constrain, the
    % third is not a geometric constraint: no lambda
    elconstr(k,1) = disk_loc_pos(3);
    elconstrdqa(k,k) = ddisk_loc_posdqa(3);
    elconstrdqe1(k, 1+Nftot*(k-1):Nftot*k) = ddisk_loc_posdq1(3,:);
    elconstrdqe2(k, 1+Nftot*(k-1):Nftot*k) = ddisk_loc_posdq2(3,:);
    elconstrdLf(k,k) = ddisk_loc_posdLf(3);
    elconstrdqd(k,:) = dgeomposddqd(3,:);
end

%% PASSIVE LEG

% extract variables
p0 = basepoints(:,7);
p1 = diskpoints(:,7);
pp2 = Rp*p1;
pL2 = pdisk +pp2;
dposdiskdrot = [dRddrolld*p1,dRddpitchd*p1,dRddyawd*p1];
h0 = eul2quat([baseangles(7),0,0],'ZYX')';
wrench = C2*lambdapassive; % mx my mz fx fy fz in local tip frame
% integrate forward
y0F = [p0;h0;zeros(3+3*Nftot,1);zeros(4+4*Nftot,1)];
fun = @(s,y) OdefunAssumedForward(s,y,qepassive,Nf,Lp,'variable');
[~,y] = ode45(fun,[0,1],y0F);
ygeom = y(end,:)';

% extract results
pL = ygeom(1:3,1);
hL = ygeom(4:7,1);
dpLdqepass = reshape(ygeom(1+7+3:7+3+3*Nftot,1),3,Nftot);
dhLdqepass = reshape(ygeom(1+7+3+3*Nftot+4:7+3+3*Nftot+4+4*Nftot,1),4,Nftot);

% integrate backward
y0B = [wrench;zeros(Nftot,1);zeros(6+Nftot,1);zeros(Nftot*(6+Nftot),1);reshape(eye(6),6*6,1);zeros(6*Nftot,1)];

% integration
funbackward = @(s,y) OdefunAssumeBackward(s,y,qepassive,wd,Nf,Lp,'variable');
[~,y] = ode45(funbackward,[1,0],y0B);
yforces = y(end,:)';

% extract results
Qc = yforces(1+6:6+Nftot,1);  
dQcdqepass = reshape(yforces(1+6+Nftot+6+Nftot+6*Nftot:6+Nftot+6+Nftot+6*Nftot+Nftot*Nftot,1),Nftot,Nftot);
dQcdw0pass = reshape(yforces(1+6+Nftot+6+Nftot+Nftot*(6+Nftot)+6*6:6+Nftot+6+Nftot+Nftot*(6+Nftot)+6*(6+Nftot)),Nftot,6);

% platform equibribrium contributions
Rtip = quat2rotmatrix(hL);

Adg = [Rtip,zeros(3); zeros(3), Rtip]; % ONLY Rotate in global frame
gwrench = Adg*(-wrench); % the wrench is the opposite wrt the one of the beam
[D1,D2,D3] = derivativeColRotMatQuat(hL);
matr1 = [wrench(1:3,1)'*D1;wrench(1:3,1)'*D2;wrench(1:3,1)'*D3;wrench(4:6,1)'*D1;wrench(4:6,1)'*D2;wrench(4:6,1)'*D3;];
dgwrenchdqepass = -matr1*dhLdqepass;
Adg2 = [eye(3),+skew(pp2); zeros(3),eye(3)]; %  translate to platform origin

wrencheqd = wrencheqd + Adg2*gwrench; % equilibrium in platform frame
wqddqepass = Adg2*dgwrenchdqepass;
wqddlampass = -Adg2*Adg*C2;
mat1 = [skew(dposdiskdrot(:,1))*gwrench(4:6,1),skew(dposdiskdrot(:,2))*gwrench(4:6,1),skew(dposdiskdrot(:,3))*gwrench(4:6,1)];
wqddqd = wqddqd + [zeros(3,3),mat1;zeros(3,6)];
    
% equations
beameqpassive = Lp*Kee*qepassive + Qc;
dbeameqpassdqepass = Lp*Kee + dQcdqepass;
dbeameqpassdlampass = dQcdw0pass*C2;
[angerr,~,dangerrdqepass] = roterr(Rd,Rtip,hL,zeros(4,1),dhLdqepass);
geomconstrpassive  = [angerr(1:2);pL-pL2;];
geompassdqepass = [dangerrdqepass(1:2,:);dpLdqepass;];
dangerrdangd = [invskew(dRddrolld'*Rtip-Rtip'*dRddrolld),invskew(dRddpitchd'*Rtip-Rtip'*dRddpitchd),invskew(dRddyawd'*Rtip-Rtip'*dRddyawd)];
geompassdqd = [ zeros(2,3),-dangerrdangd(1:2,:);-eye(3),-dposdiskdrot;]; 




%% COLLECT EQUATIONS
% ----------------------------------------------
beameq =[beameq1;beameq2;beameqpassive;elconstr];
gradbeameq = [dbeameq1dqa,    dbeameq1dqe1,         dbeameq1dqe2,         zeros(6*Nftot,Nftot), dbeameq1dLf,    zeros(6*Nftot,6), zeros(6*Nftot,6), dbeameq1dlamp,    dbeameq1dlamd,    zeros(6*Nftot,5);
              dbeameq2dqa,    dbeameq2dqe1,         dbeameq2dqe2,         zeros(6*Nftot,Nftot), dbeameq2dLf,    zeros(6*Nftot,6), zeros(6*Nftot,6), dbeameq2dlamp,    dbeameq2dlamd,    zeros(6*Nftot,5);
              zeros(Nftot,6), zeros(Nftot,6*Nftot), zeros(Nftot,6*Nftot), dbeameqpassdqepass,   zeros(Nftot,6), zeros(Nftot,6),   zeros(Nftot,6),   zeros(Nftot,5*6), zeros(Nftot,4*6), dbeameqpassdlampass;
              elconstrdqa,    elconstrdqe1,         elconstrdqe2,         zeros(6,Nftot),       elconstrdLf,    elconstrdqd,      zeros(6,6),       zeros(6,5*6),     zeros(6,4*6),     zeros(6,5)     ];

equilibriums = [wrencheqp;wrencheqd];
gradequilibrium = [wqpdqa, wqpdqe1, wqpdqe2, zeros(6,Nftot), wqpdLf, zeros(6,6), wqpdqp,     wqpdlamp, zeros(6,4*6), zeros(6,5);
                   wqddqa, wqddqe1, wqddqe2, wqddqepass,     wqddLf, wqddqd,     zeros(6,6), wqddlamp, wqddlamd,     wqddlampass;];
% rotate for correct singu
equilibriums = [equilibriums(4:6,1);equilibriums(1:3,1);equilibriums(10:12,1);equilibriums(7:9,1)];
gradequilibrium = [gradequilibrium(4:6,:);gradequilibrium(1:3,:);gradequilibrium(10:12,:);gradequilibrium(7:9,:)];

geomconstr = [geomconstrp;geomconstrd;geomconstrpassive];
gradgeom = [geompdqa,   geompdqe1,        geompdqe2,        zeros(5*6,Nftot), geompdLf,   zeros(5*6,6), geompdqp,     zeros(5*6,5*6), zeros(5*6,4*6), zeros(5*6,5);
            geomddqa,   geomddqe1,        geomddqe2,        zeros(4*6,Nftot), geomddLf,   geomddqd,     zeros(4*6,6), zeros(4*6,5*6), zeros(4*6,4*6), zeros(4*6,5);
            zeros(5,6), zeros(5,6*Nftot), zeros(5,6*Nftot), geompassdqepass,  zeros(5,6), geompassdqd,  zeros(5,6),   zeros(5,5*6),   zeros(5,4*6),   zeros(5,5);];

% inverse problem

inv = qp-qpd;
gradinv = [zeros(6,6),zeros(6,6*Nftot*2+Nftot+6+6),eye(6),zeros(6,6*5+6*4+5)];

% collect

eq = [beameq;equilibriums;geomconstr;inv];

gradeq = [gradbeameq;gradequilibrium;gradgeom;gradinv];
end