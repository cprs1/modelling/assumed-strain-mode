function [flag1,flag2] = SingularityMode3RFS(jac,Nfm)

Nf = 3*Nfm;

A1 = jac(1:3*Nf+6,1:3);
U1 = jac(1:3*Nf+6,1+3:3+3*Nf);
P1 = jac(1:3*Nf+6,1+3+3*Nf:3+3*Nf+6);

A2 = jac(1+3*Nf+6:3*Nf+6+3*3,1:3);
U2 = jac(1+3*Nf+6:3*Nf+6+3*3,1+3:3+3*Nf);
P2 = jac(1+3*Nf+6:3*Nf+6+3*3,1+3+3*Nf:3+3*Nf+6);

G = jac(1:3*Nf+6,1+3+3*Nf+6:3+6+3*Nf+3*3);
Z = null(G');

T1 = [Z'*A1,Z'*U1, Z'*P1(:,4:6);
         A2,   U2, P2(:,4:6)];
     
T2 = [Z'*P1,Z'*U1; 
         P2,   U2];

flag1 = rcond(T1);
flag2 = rcond(T2);

end