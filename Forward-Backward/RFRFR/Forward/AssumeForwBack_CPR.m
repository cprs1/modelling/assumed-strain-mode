%% Forward Geometrico Static Problem
% Strong form solved with shooting method
% RFRFR Robot


clear
close all
clc

%%
r = 0.001;
E = 210*10^9;
L = 1;
A = pi*r^2;
I = 0.25*pi*r^4;
Kbt = E*I;
g = [0;0];
rho = 7800;
f = rho*A*g;
fext = [0;.0];

wp = fext; % RFRFR no moments on EE
wd = [0;f]; % distributed wrench


%% Inverse problem data

qa = [120;60]*pi/180;
qad = qa;

%% GEOMETRY
rAB = 0.4;

basepoints = [-rAB/2,+rAB/2;0,0];
geometry.basepoints = basepoints;


%% Solution
Nf = 4;
qa0 = qa;
qe0 = zeros(2*Nf,1);
qp0 = [0;0.8];
lambda0 = zeros(4,1);
guess0 = [qa0;qe0;qp0;lambda0];
options = optimoptions('fsolve','display','iter-detailed','Algorithm','trust-region','Maxiter',50,'SpecifyObjectiveGradient',true,'CheckGradients',false);

Phi = @(s) BaseFcnLegendre(s,1,Nf)';
Kad = integral(@(s) Phi(s)'*Kbt*Phi(s),0,1,'ArrayValued',true);

fun = @(guess) ForwardModeForwBackRFRFSeqn(guess,geometry,Kad,L,wp,wd,qad,Nf);
[sol,~,~,~,jac] = fsolve(fun,guess0,options);

%% PLOT
Nsh = 100;
s_span = linspace(0,L,Nsh);
posbeams = zeros(4,Nsh);
qe =  sol(1+2:2+2*Nf,1);
qa = sol(1:2);
qp = sol(1+2+2*Nf:2+2*Nf+2,1);

pplat = qp;

Lspan = [0,1];

for i = 1:2
    qai = qa(i);
    qei = qe(1+Nf*(i-1):Nf*i,1);
    fun = @(s,y) OdeFunReconstruct(s,y,qei,Nf,L);
    p0 = basepoints(:,i);
    th0 = qai;
    y0 = [p0;th0];
    [si,yi] = ode45(fun,Lspan,y0);
    posbeams(1+2*(i-1):2*i,:) = spline(L*si,yi(:,1:2)',s_span);
end
PlotRFRFR(posbeams(1:2,:),posbeams(3:4,:),L)
title('Assumed Mode')
axis equal
