function yd = OdefunAssumedMode(s,y,qei,f,Nf,L,str)
Nftot = 3*Nf;

switch str
    %% FIXED LENGTH CASE
    case 'fix'
        % extract variables
        y1 = y(1:7+8*Nftot,1);
        y2 = y(1+7+8*Nftot:14+16*Nftot,1);
        y3 = y(1+14+16*Nftot:end,1);
        
        h = y1(1:4,1);
        Jh = reshape(y1(1+7:7+4*Nftot,1),4,Nftot);
        Jp = reshape(y1(1+7+4*Nftot:7+4*Nftot+3*Nftot,1),3,Nftot);
        
        dhdqa = y2(1:4,1);
        dJhdqa = reshape(y2(1+7:7+4*Nftot,1),4,Nftot);
        dJpdqa = reshape(y2(1+7+4*Nftot:7+4*Nftot+3*Nftot,1),3,Nftot);
        
        dJhdqei = reshape(y3(1:4*Nftot*Nftot,1),4*Nftot,Nftot);
        dJpdqei = reshape(y3(1+4*Nftot*Nftot:4*Nftot*Nftot+3*Nftot*Nftot,1),3*Nftot,Nftot);
        
        % compute intermediate terms
        d3 = [2*(h(1)*h(3)+h(2)*h(4));2*(h(3)*h(4)-h(1)*h(2));h(1)^2-h(2)^2-h(3)^2+h(4)^2];
        Phi =  PhiMatr(s,1,Nf);
        k = Phi*qei;
        A = [0,-k(1),-k(2),-k(3);
            +k(1),0,+k(3),-k(2);
            +k(2),-k(3),0,+k(1);
            +k(3),+k(2),-k(1),0;];
        
        % usefull matrices
        Dk = [-h(2),-h(3),-h(4);+h(1),-h(4),+h(3);+h(4),+h(1),-h(2);-h(3),+h(2),+h(1)];
        Dh = 2*[+h(3),+h(4),+h(1),+h(2);-h(2),-h(1),+h(4),+h(3);+h(1),-h(2),-h(3),+h(4)];

        % first derivatives -----------------

        hd = 0.5*A*h;
        pd = d3;
        Jhd = 0.5*(Dk*Phi+A*Jh);
        Jpd = Dh*Jh;
        Qwd = f'*Jp;
        
        % deriv wrt qa --------------------
        dhdqad = 0.5*A*dhdqa;
        dpdqad = Dh*dhdqa;
        
        
        dJhdqad1 = [-Phi(1,:)*dhdqa(2,1)-Phi(2,:)*dhdqa(3,1)-Phi(3,:)*dhdqa(4,1);
            +Phi(1,:)*dhdqa(1,1)+Phi(3,:)*dhdqa(3,1)-Phi(2,:)*dhdqa(4,1);
            +Phi(2,:)*dhdqa(1,1)-Phi(3,:)*dhdqa(2,1)+Phi(1,:)*dhdqa(4,1);
            +Phi(3,:)*dhdqa(1,1)+Phi(2,:)*dhdqa(2,1)-Phi(1,:)*dhdqa(3,1);];
        
        dJhdqad2 = [-k(1,1)*dJhdqa(2,:)-k(2,1)*dJhdqa(3,:)-k(3,1)*dJhdqa(4,:);
            +k(1,1)*dJhdqa(1,:)+k(3,1)*dJhdqa(3,:)-k(2,1)*dJhdqa(4,:);
            +k(2,1)*dJhdqa(1,:)-k(3,1)*dJhdqa(2,:)+k(1,1)*dJhdqa(4,:);
            +k(3,1)*dJhdqa(1,:)+k(2,1)*dJhdqa(2,:)-k(1,1)*dJhdqa(3,:);];
        dJhdqad = 0.5*(dJhdqad1 + dJhdqad2);
        
        dJpdqad1 = [+Jh(3,:)*dhdqa(1,1)+Jh(4,:)*dhdqa(2,1)+Jh(1,:)*dhdqa(3,1)+Jh(2,:)*dhdqa(4,1);
            -Jh(2,:)*dhdqa(1,1)-Jh(1,:)*dhdqa(2,1)+Jh(4,:)*dhdqa(3,1)+Jh(3,:)*dhdqa(4,1);
            +Jh(1,:)*dhdqa(1,1)-Jh(2,:)*dhdqa(2,1)-Jh(3,:)*dhdqa(3,1)+Jh(4,:)*dhdqa(4,1);];
        
        dJpdqad2 = [+h(3,1)*dJhdqa(1,:)+h(4,1)*dJhdqa(2,:)+h(1,1)*dJhdqa(3,:)+h(2,1)*dJhdqa(4,:);
            -h(2,1)*dJhdqa(1,:)-h(1,1)*dJhdqa(2,:)+h(4,1)*dJhdqa(3,:)+h(3,1)*dJhdqa(4,:);
            +h(1,1)*dJhdqa(1,:)-h(2,1)*dJhdqa(2,:)-h(3,1)*dJhdqa(3,:)+h(4,1)*dJhdqa(4,:);];
        
        dJpdqad = 2*(dJpdqad1 + dJpdqad2);
        
        dQwdqad = f'*dJpdqa;
         
        % deriv wrt qe ----------------------------------------
        fdiag = zeros(3*Nftot,Nftot);
        Aphidiag = zeros(4*Nftot,4);
        Chdiag = zeros(4*Nftot,3);
        Adiag = zeros(4*Nftot,4*Nftot);
        Bhdiag = zeros(3*Nftot,4);
        Dhdiag = zeros(3*Nftot,4*Nftot);
        
        % ASSEMBLING MATRICES
        for i = 1:Nftot
            
            Aphidiag(1+4*(i-1):4*i,:) = [0,-Phi(1,i),-Phi(2,i),-Phi(3,i);
                +Phi(1,i),0,+Phi(3,i),-Phi(2,i);
                +Phi(2,i),-Phi(3,i),0,+Phi(1,i);
                +Phi(3,i),+Phi(2,i),-Phi(1,i),0;];
            
            Chdiag(1+4*(i-1):4*i,:) = [-Jh(2,i),-Jh(3,i),-Jh(4,i);
                +Jh(1,i),-Jh(4,i),+Jh(3,i);
                +Jh(4,i),+Jh(1,i),-Jh(2,i);
                -Jh(3,i),+Jh(2,i),+Jh(1,i)];
            
            Adiag(1+4*(i-1):4*i,1+4*(i-1):4*i) = A;
            
            Bhdiag(1+3*(i-1):3*i,:) = 2*[+Jh(3,i),+Jh(4,i),+Jh(1,i),+Jh(2,i);
                -Jh(2,i),-Jh(1,i),+Jh(4,i),+Jh(3,i);
                +Jh(1,i),-Jh(2,i),-Jh(3,i),+Jh(4,i)];
            
            Dhdiag(1+3*(i-1):3*i,1+4*(i-1):4*i) = Dh;
            
            fdiag(1+3*(i-1):3*i,i) = f;
        end
        
        dJhdqeid = 0.5*(Aphidiag*Jh + Chdiag*Phi + Adiag*dJhdqei);
        dJpdqeid = Bhdiag*Jh + Dhdiag*dJhdqei;
        dQwdqeid = dJpdqei'*fdiag;
        
        % collect
        yd1 = [hd;pd;reshape(Jhd,4*Nftot,1);reshape(Jpd,3*Nftot,1);reshape(Qwd,Nftot,1)];
        yd2 = [dhdqad;dpdqad;reshape(dJhdqad,4*Nftot,1);reshape(dJpdqad,3*Nftot,1);reshape(dQwdqad,Nftot,1)];
        yd3 = [reshape(dJhdqeid,4*Nftot*Nftot,1);reshape(dJpdqeid,3*Nftot*Nftot,1);reshape(dQwdqeid,Nftot*Nftot,1)];
        yd = L*[yd1;yd2;yd3];
        
    %% VARIABLE LENGTH CASE
    case 'variable'
        % extract variables
        y1 = y(1:7+8*Nftot,1);
        y2 = y(1+7+8*Nftot:14+16*Nftot,1);
        y3 = y(1+14+16*Nftot:end,1);
        
        h = y1(1:4,1);
        Jh = reshape(y1(1+7:7+4*Nftot,1),4,Nftot);
        Jp = reshape(y1(1+7+4*Nftot:7+4*Nftot+3*Nftot,1),3,Nftot);
        
        dhdqa = y2(1:4,1);
        dJhdqa = reshape(y2(1+7:7+4*Nftot,1),4,Nftot);
        dJpdqa = reshape(y2(1+7+4*Nftot:7+4*Nftot+3*Nftot,1),3,Nftot);
        
        dJhdqei = reshape(y3(1:4*Nftot*Nftot,1),4*Nftot,Nftot);
        dJpdqei = reshape(y3(1+4*Nftot*Nftot:4*Nftot*Nftot+3*Nftot*Nftot,1),3*Nftot,Nftot);
        
        % compute intermediate terms
        d3 = [2*(h(1)*h(3)+h(2)*h(4));2*(h(3)*h(4)-h(1)*h(2));h(1)^2-h(2)^2-h(3)^2+h(4)^2];
        Phi =  PhiMatr(s,1,Nf);
        k = Phi*qei;
        A = [0,-k(1),-k(2),-k(3);
            +k(1),0,+k(3),-k(2);
            +k(2),-k(3),0,+k(1);
            +k(3),+k(2),-k(1),0;];
        
        % usefull matrices
        Dk = [-h(2),-h(3),-h(4);+h(1),-h(4),+h(3);+h(4),+h(1),-h(2);-h(3),+h(2),+h(1)];
        Dh = 2*[+h(3),+h(4),+h(1),+h(2);-h(2),-h(1),+h(4),+h(3);+h(1),-h(2),-h(3),+h(4)];

        % first derivatives -----------------
        hd = L*0.5*A*h;
        pd = L*d3;
        Jhd = L*0.5*(Dk*Phi+A*Jh);
        Jpd = L*Dh*Jh;
        Qwd = L*f'*Jp;
        
        % deriv. wrt qa ------------------
        dhdqad = 0.5*A*(L*dhdqa+h);
        dpdqad = L*Dh*dhdqa+d3;
        
        dJhdqad1 = [-Phi(1,:)*dhdqa(2,1)-Phi(2,:)*dhdqa(3,1)-Phi(3,:)*dhdqa(4,1);
            +Phi(1,:)*dhdqa(1,1)+Phi(3,:)*dhdqa(3,1)-Phi(2,:)*dhdqa(4,1);
            +Phi(2,:)*dhdqa(1,1)-Phi(3,:)*dhdqa(2,1)+Phi(1,:)*dhdqa(4,1);
            +Phi(3,:)*dhdqa(1,1)+Phi(2,:)*dhdqa(2,1)-Phi(1,:)*dhdqa(3,1);];
        
        dJhdqad2 = [-k(1,1)*dJhdqa(2,:)-k(2,1)*dJhdqa(3,:)-k(3,1)*dJhdqa(4,:);
            +k(1,1)*dJhdqa(1,:)+k(3,1)*dJhdqa(3,:)-k(2,1)*dJhdqa(4,:);
            +k(2,1)*dJhdqa(1,:)-k(3,1)*dJhdqa(2,:)+k(1,1)*dJhdqa(4,:);
            +k(3,1)*dJhdqa(1,:)+k(2,1)*dJhdqa(2,:)-k(1,1)*dJhdqa(3,:);];
        dJhdqad = 0.5*(Dk*Phi+A*Jh) + 0.5*L*(dJhdqad1 + dJhdqad2);
        
        dJpdqad1 = [+Jh(3,:)*dhdqa(1,1)+Jh(4,:)*dhdqa(2,1)+Jh(1,:)*dhdqa(3,1)+Jh(2,:)*dhdqa(4,1);
            -Jh(2,:)*dhdqa(1,1)-Jh(1,:)*dhdqa(2,1)+Jh(4,:)*dhdqa(3,1)+Jh(3,:)*dhdqa(4,1);
            +Jh(1,:)*dhdqa(1,1)-Jh(2,:)*dhdqa(2,1)-Jh(3,:)*dhdqa(3,1)+Jh(4,:)*dhdqa(4,1);];
        
        dJpdqad2 = [+h(3,1)*dJhdqa(1,:)+h(4,1)*dJhdqa(2,:)+h(1,1)*dJhdqa(3,:)+h(2,1)*dJhdqa(4,:);
            -h(2,1)*dJhdqa(1,:)-h(1,1)*dJhdqa(2,:)+h(4,1)*dJhdqa(3,:)+h(3,1)*dJhdqa(4,:);
            +h(1,1)*dJhdqa(1,:)-h(2,1)*dJhdqa(2,:)-h(3,1)*dJhdqa(3,:)+h(4,1)*dJhdqa(4,:);];
        
        dJpdqad = Dh*Jh + 2*L*(dJpdqad1 + dJpdqad2);
        
        dQwdqad = L*f'*dJpdqa + f'*Jp;
          
        % deriv wrt qe -------------------------------
        fdiag = zeros(3*Nftot,Nftot);
        Aphidiag = zeros(4*Nftot,4);
        Chdiag = zeros(4*Nftot,3);
        Adiag = zeros(4*Nftot,4*Nftot);
        Bhdiag = zeros(3*Nftot,4);
        Dhdiag = zeros(3*Nftot,4*Nftot);
        
        % ASSEMBLING MATRICES
        for i = 1:Nftot
            
            Aphidiag(1+4*(i-1):4*i,:) = [0,-Phi(1,i),-Phi(2,i),-Phi(3,i);
                +Phi(1,i),0,+Phi(3,i),-Phi(2,i);
                +Phi(2,i),-Phi(3,i),0,+Phi(1,i);
                +Phi(3,i),+Phi(2,i),-Phi(1,i),0;];
            
            Chdiag(1+4*(i-1):4*i,:) = [-Jh(2,i),-Jh(3,i),-Jh(4,i);
                +Jh(1,i),-Jh(4,i),+Jh(3,i);
                +Jh(4,i),+Jh(1,i),-Jh(2,i);
                -Jh(3,i),+Jh(2,i),+Jh(1,i)];
            
            Adiag(1+4*(i-1):4*i,1+4*(i-1):4*i) = A;
            
            Bhdiag(1+3*(i-1):3*i,:) = 2*[+Jh(3,i),+Jh(4,i),+Jh(1,i),+Jh(2,i);
                -Jh(2,i),-Jh(1,i),+Jh(4,i),+Jh(3,i);
                +Jh(1,i),-Jh(2,i),-Jh(3,i),+Jh(4,i)];
            
            Dhdiag(1+3*(i-1):3*i,1+4*(i-1):4*i) = Dh;
            
            fdiag(1+3*(i-1):3*i,i) = f;
        end
        
        dJhdqeid = 0.5*L*(Aphidiag*Jh + Chdiag*Phi + Adiag*dJhdqei);
        dJpdqeid = L*(Bhdiag*Jh + Dhdiag*dJhdqei);
        dQwdqeid = L*(dJpdqei'*fdiag);
        
        % collect ------------
        yd1 = [hd;pd;reshape(Jhd,4*Nftot,1);reshape(Jpd,3*Nftot,1);reshape(Qwd,Nftot,1)];
        yd2 = [dhdqad;dpdqad;reshape(dJhdqad,4*Nftot,1);reshape(dJpdqad,3*Nftot,1);reshape(dQwdqad,Nftot,1)];
        yd3 = [reshape(dJhdqeid,4*Nftot*Nftot,1);reshape(dJpdqeid,3*Nftot*Nftot,1);reshape(dQwdqeid,Nftot*Nftot,1)];
        yd = [yd1;yd2;yd3];
    otherwise
        error('Define correctly string')
end

end