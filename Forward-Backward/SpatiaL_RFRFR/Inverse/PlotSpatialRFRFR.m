function h = PlotSpatialRFRFR(geometry,qa,posbeams,pplat,Rplat)
basepoints = geometry.basepoints;
% figure()
% plot fixed frames
yvect = [0;1;0]/5;
xvect = [1;0;0]/5;
zvect = [0;0;1]/5;
quiver3(0,0,0,xvect(1),xvect(2),xvect(3),'Color','r','LineWidth',1.5)
hold on
quiver3(0,0,0,yvect(1),yvect(2),yvect(3),'Color','b','LineWidth',1.5)
quiver3(0,0,0,zvect(1),zvect(2),zvect(3),'Color','k','LineWidth',1.5)

h = [];
for i = 1:2
   plot3(basepoints(1,i),basepoints(2,i),basepoints(3,i),'bo','LineWidth',1.5)
   xlocal = Rx(qa(i,1))*xvect;
   ylocal = Rx(qa(i,1))*yvect;
   zlocal = Rx(qa(i,1))*zvect;
   h2 = quiver3(basepoints(1,i),basepoints(2,i),basepoints(3,i),xlocal(1),xlocal(2),xlocal(3),'Color','r','LineWidth',1.5);
   h3 = quiver3(basepoints(1,i),basepoints(2,i),basepoints(3,i),ylocal(1),ylocal(2),ylocal(3),'Color','b','LineWidth',1.5);
   h4 = quiver3(basepoints(1,i),basepoints(2,i),basepoints(3,i),zlocal(1),zlocal(2),zlocal(3),'Color','k','LineWidth',1.5);
   yi = posbeams(1+3*(i-1):3*i,:);
   h5 = plot3(yi(1,:),yi(2,:),yi(3,:),'k','LineWidth',1.5);
   h = [h;h2;h3;h4;h5];
end

% plot platform frame
xplat = Rplat*xvect;
yplat = Rplat*yvect;
zplat = Rplat*zvect;
h1 = quiver3(pplat(1),pplat(2),pplat(3),xplat(1),xplat(2),xplat(3),'Color','r','LineWidth',1.5);
h2 = quiver3(pplat(1),pplat(2),pplat(3),yplat(1),yplat(2),yplat(3),'Color','b','LineWidth',1.5);
h3 = quiver3(pplat(1),pplat(2),pplat(3),zplat(1),zplat(2),zplat(3),'Color','k','LineWidth',1.5);


pB1 = posbeams(1:3,end);
pB2 = posbeams(4:6,end);
h4 = plot3(pplat(1,1),pplat(2,1),pplat(3,1),'r*','LineWidth',2);
h5 = line([pB1(1),pB2(1)],[pB1(2),pB2(2)],[pB1(3),pB2(3)],'Color','k','LineWidth',1.5);
h = [h;h1;h2;h3;h4;h5];

axis equal
axis([-1 1 -1 1 -1 1])
grid on

xlabel('x')
ylabel('y')
zlabel('z')
end