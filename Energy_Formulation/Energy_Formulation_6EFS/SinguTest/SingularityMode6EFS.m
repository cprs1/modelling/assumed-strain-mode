function [flag1,flag2] = SingularityMode6EFS(jac,Nfm)

Nf = 3*Nfm;

A1 = jac(1:6*Nf+6,1:6);
U1 = jac(1:6*Nf+6,1+6:6+6*Nf);
P1 = jac(1:6*Nf+6,1+6+6*Nf:6+6*Nf+6);

A2 = jac(1+6*Nf+6:6*Nf+6+3*6,1:6);
U2 = jac(1+6*Nf+6:6*Nf+6+3*6,1+6:6+6*Nf);
P2 = jac(1+6*Nf+6:6*Nf+6+3*6,1+6+6*Nf:6+6*Nf+6);

G = jac(1:6*Nf+6,1+6+6*Nf+6:6+6+6*Nf+6*3);
Z = null(G');

T1 = [Z'*A1,Z'*U1;
         A2,   U2];
     
T2 = [Z'*P1,Z'*U1; 
         P2,   U2];

flag1 = rcond(T1);
flag2 = rcond(T2);

end