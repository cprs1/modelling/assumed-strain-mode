function flag = StabilityMode3RFS(jac,Nfm)
Nf = 3*Nfm;


U =   jac(1:3*Nf+6,1+3     :3+3*Nf);
P = jac(1:3*Nf+6,1+3+3*Nf:3+3*Nf+6);

G = jac(1:3*Nf+6,1+3+3*Nf+6:3+3*Nf+6+3*3);
Z = null(G');

H = [U,P];
Hr = Z'*H*Z;

[~,flag] = chol(Hr);

end