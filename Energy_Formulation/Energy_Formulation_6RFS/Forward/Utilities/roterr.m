function [err,derrdqa,derrdqe] = roterr(Rp,Rt,h,dhdqa,dhdqe)

errx = Rp(1,2)*Rt(1,3)+Rp(2,2)*Rt(2,3)+Rp(3,2)*Rt(3,3)-Rp(1,3)*Rt(1,2)-Rp(2,3)*Rt(2,2)-Rp(3,3)*Rt(3,2);
erry = Rp(1,3)*Rt(1,1)+Rp(2,3)*Rt(2,1)+Rp(3,3)*Rt(3,1)-Rp(1,1)*Rt(1,3)-Rp(2,1)*Rt(2,3)-Rp(3,1)*Rt(3,3);
errz = Rp(1,1)*Rt(1,2)+Rp(2,1)*Rt(2,2)+Rp(3,1)*Rt(3,2)-Rp(1,2)*Rt(1,1)-Rp(2,2)*Rt(2,1)-Rp(3,2)*Rt(3,1);

err = [errx;erry;errz];

derrdqa = [];
derrdqe = [];

if nargin>2
    [D1,D2,D3] = derivativeColRotMatQuat(h);
    dd1dqa = D1*dhdqa;
    dd2dqa = D2*dhdqa;
    dd3dqa = D3*dhdqa;
    dd1dqe = D1*dhdqe;
    dd2dqe = D2*dhdqe;
    dd3dqe = D3*dhdqe;
    derrxdqa = Rp(1,2)*dd1dqa(3,:)+Rp(2,2)*dd2dqa(3,:)+Rp(3,2)*dd3dqa(3,:)-Rp(1,3)*dd1dqa(2,:)-Rp(2,3)*dd2dqa(2,:)-Rp(3,3)*dd3dqa(2,:);
    derrxdqe = Rp(1,2)*dd1dqe(3,:)+Rp(2,2)*dd2dqe(3,:)+Rp(3,2)*dd3dqe(3,:)-Rp(1,3)*dd1dqe(2,:)-Rp(2,3)*dd2dqe(2,:)-Rp(3,3)*dd3dqe(2,:);
    derrydqa = Rp(1,3)*dd1dqa(1,:)+Rp(2,3)*dd2dqa(1,:)+Rp(3,3)*dd3dqa(1,:)-Rp(1,1)*dd1dqa(3,:)-Rp(2,1)*dd2dqa(3,:)-Rp(3,1)*dd3dqa(3,:);
    derrydqe = Rp(1,3)*dd1dqe(1,:)+Rp(2,3)*dd2dqe(1,:)+Rp(3,3)*dd3dqe(1,:)-Rp(1,1)*dd1dqe(3,:)-Rp(2,1)*dd2dqe(3,:)-Rp(3,1)*dd3dqe(3,:);
    derrzdqa = Rp(1,1)*dd1dqa(2,:)+Rp(2,1)*dd2dqa(2,:)+Rp(3,1)*dd3dqa(2,:)-Rp(1,2)*dd1dqa(1,:)-Rp(2,2)*dd2dqa(1,:)-Rp(3,2)*dd3dqa(1,:);
    derrzdqe = Rp(1,1)*dd1dqe(2,:)+Rp(2,1)*dd2dqe(2,:)+Rp(3,1)*dd3dqe(2,:)-Rp(1,2)*dd1dqe(1,:)-Rp(2,2)*dd2dqe(1,:)-Rp(3,2)*dd3dqe(1,:);
    derrdqa = [derrxdqa;derrydqa;derrzdqa];
    derrdqe = [derrxdqe;derrydqe;derrzdqe];
end