function [eq,gradeq] = InverseMode6EFR_1p(guess,geometry,Kee,qpd,wp,wd,Nf)
Nftot = 3*Nf;

basepoints = geometry.basepoints;
platpoints = geometry.platpoints;
baseangles = geometry.baseangles;
rotparam = geometry.rotparam;

qa = guess(1:6,1);
qe = guess(1+6:6+7*Nftot,1);
Lp = guess(1+6+7*Nftot,1);
qp = guess(1+6+7*Nftot+1:6+7*Nftot+1+6,1);
lambda = guess(1+6+7*Nftot+1+6:6+7*Nftot+1+6+5*7+1,1);

pplat = qp(1:3,1);
[Rp,dRpdroll,dRpdpitch,dRpdyaw] = rotationParametrization(qp(4:6,1),rotparam);


Ci = [eye(2),zeros(2,3);zeros(1,5);zeros(3,2),eye(3)];

% matrix initialization
beameq = zeros(7*Nftot,1);
dbeamdqa = zeros(7*Nftot,6);
dbeamdqe = zeros(7*Nftot,7*Nftot);
dbeamdlambd = zeros(7*Nftot,5*7+1);
geomconstr = zeros(5*7+1,1);
dconsdqa = zeros(5*7+1,6);
dconsdqe = zeros(5*7+1,7*Nftot);
dconsdqp = zeros(5*7,6);

wrencheq = wp; % platform frame equilibrium
dwrenchdqa = zeros(6,6);
dwrenchdqe = zeros(6,7*Nftot);

dwrenchdqp = zeros(6,6);
dwrenchdlambd = zeros(6,5*7+1);


for k = 1:6
    % BEAM  INTEGRATION
    % extract variables
    L = qa(k,1);
    p0 = basepoints(:,k);
    p1 = platpoints(:,k);
    pp = Rp*platpoints(:,k);
    pLp = pplat +pp;
    dppdrot = [dRpdroll*p1,dRpdpitch*p1,dRpdyaw*p1];

    h0 = eul2quat([baseangles(k),0,0],'ZYX')';
    qei = qe(1+Nftot*(k-1):k*Nftot,1);
    lambdai = lambda(1+5*(k-1):5*k,1); % mx my fx fy fz in local tip frame
    wrench = (Ci*lambdai);
    
    % integrate forward
    y0F = [p0;h0;zeros(3+3*Nftot,1);zeros(4+4*Nftot,1)];
    fun = @(s,y) OdefunAssumedForward(s,y,qei,Nf,L,'variable');
    [~,y] = ode45(fun,[0,1],y0F);
    ygeom = y(end,:)';
    
    % extract results
    pL = ygeom(1:3,1);
    hL = ygeom(4:7,1);
    dpLdqa = ygeom(1+7:7+3,1);
    dpLdqe = reshape(ygeom(1+7+3:7+3+3*Nftot,1),3,Nftot);
    dhLdqa = ygeom(1+7+3+3*Nftot:7+3+3*Nftot+4,1);
    dhLdqe = reshape(ygeom(1+7+3+3*Nftot+4:7+3+3*Nftot+4+4*Nftot,1),4,Nftot);
    
    % integrate backward
    y0B = [wrench;zeros(Nftot,1);zeros(6+Nftot,1);zeros(Nftot*(6+Nftot),1);reshape(eye(6),6*6,1);zeros(6*Nftot,1)];

    % integration
    funbackward = @(s,y) OdefunAssumeBackward(s,y,qei,wd,Nf,L,'variable');
    [~,y] = ode45(funbackward,[1,0],y0B);
    yforces = y(end,:)';
    
    % extract results
    Qc =   yforces(1+6:6+Nftot,1);
    dQcdqa = yforces(1+6+Nftot+6:6+Nftot+6+Nftot,1);
    dQcdqe = reshape(yforces(1+6+Nftot+6+Nftot+6*Nftot:6+Nftot+6+Nftot+6*Nftot+Nftot*Nftot,1),Nftot,Nftot);
    dQcdw0 = reshape(yforces(1+6+Nftot+6+Nftot+Nftot*(6+Nftot)+6*6:6+Nftot+6+Nftot+Nftot*(6+Nftot)+6*(6+Nftot)),Nftot,6);
    
    % platform equibribrium contributions
    Rtip = quat2rotmatrix(hL);
    
    Adg = [Rtip,zeros(3); zeros(3), Rtip]; % ONLY Rotate in global frame
    gwrench = Adg*(-wrench); % the wrench is the opposite wrt the one of the beam    
    Adg2 = [eye(3),+skew(pp); zeros(3),eye(3)]; %  translate to platform origin
    
    wrencheq = wrencheq + Adg2*gwrench; % equilibrium in platform frame 
    
    [D1,D2,D3] = derivativeColRotMatQuat(hL);
    matr2 = [wrench(1:3,1)'*D1;wrench(1:3,1)'*D2;wrench(1:3,1)'*D3;wrench(4:6,1)'*D1;wrench(4:6,1)'*D2;wrench(4:6,1)'*D3;];
    dgwrenchdqa = -matr2*dhLdqa;
    dgwrenchdqe = -matr2*dhLdqe;
    mat1 = [skew(dppdrot(:,1))*gwrench(4:6,1),skew(dppdrot(:,2))*gwrench(4:6,1),skew(dppdrot(:,3))*gwrench(4:6,1)];
    dwrenchdqp = dwrenchdqp +[zeros(3,3),mat1;zeros(3,6)];
    
    % equations and gradient
    beameq(1+Nftot*(k-1):Nftot*k,1) = L*Kee*qei + Qc;
    dbeamdqe(1+Nftot*(k-1):Nftot*k,1+Nftot*(k-1):Nftot*k) = L*Kee + dQcdqe;
    dbeamdlambd(1+Nftot*(k-1):Nftot*k,1+5*(k-1):5*k) = dQcdw0*Ci;
    [angerr,dangerrdqa,dangerrdqe] = roterr(Rp,Rtip,hL,dhLdqa,dhLdqe);
    dangerrdang = [invskew(dRpdroll'*Rtip-Rtip'*dRpdroll),invskew(dRpdpitch'*Rtip-Rtip'*dRpdpitch),invskew(dRpdyaw'*Rtip-Rtip'*dRpdyaw)];
    geomconstr(1+5*(k-1):5*k,1)  = [angerr(1:2,1);pL-pLp;];
    dconsdqe(1+5*(k-1):5*k,1+Nftot*(k-1):Nftot*k) = [dangerrdqe(1:2,:);dpLdqe];
    dconsdqp(1+5*(k-1):5*k,:) = [ zeros(2,3),-dangerrdang(1:2,:);-eye(3),-dppdrot;];
    dwrenchdqe(:,1+Nftot*(k-1):Nftot*k) = Adg2*dgwrenchdqe;
    dwrenchdlambd(:,1+5*(k-1):5*k) = -Adg2*Adg*Ci;
    dbeamdqa(1+Nftot*(k-1):Nftot*k,k) = Kee*qei + dQcdqa;
    dconsdqa(1+5*(k-1):5*k,k) = [dangerrdqa(1:2,:);dpLdqa];
    dwrenchdqa(:,k) = Adg2*dgwrenchdqa;
    
end

%% PASSIVE LEG
dbeamdLp  = zeros(7*Nftot,1);
dconsdLp = zeros(5*7,1);
dwrenchdLp = zeros(6,1);
dLpeqdqe  = zeros(1,7*Nftot);
dLpeqdlambd = zeros(1,5*7+1);

% extract variables
L = Lp;
p0 = basepoints(:,7);
p1 = platpoints(:,7);
pp = Rp*p1;
pLp = pplat +pp;
dppdrot = [dRpdroll*p1,dRpdpitch*p1,dRpdyaw*p1];
h0 = eul2quat([baseangles(7),0,0],'ZYX')';
qei = qe(1+Nftot*6:7*Nftot,1);
lambdai = lambda(1+5*6:5*7+1,1); % mx my mz fx fy fz in local tip frame
wrench = lambdai;
% integrate forward
y0F = [p0;h0;zeros(3+3*Nftot,1);zeros(4+4*Nftot,1)];
fun = @(s,y) OdefunAssumedForward(s,y,qei,Nf,L,'variable');
[~,y] = ode45(fun,[0,1],y0F);
ygeom = y(end,:)';

% extract results
pL = ygeom(1:3,1);
hL = ygeom(4:7,1);
dpLdLp = ygeom(1+7:7+3,1);
dpLdqe = reshape(ygeom(1+7+3:7+3+3*Nftot,1),3,Nftot);
dhLdLp = ygeom(1+7+3+3*Nftot:7+3+3*Nftot+4,1);
dhLdqe = reshape(ygeom(1+7+3+3*Nftot+4:7+3+3*Nftot+4+4*Nftot,1),4,Nftot);
 
% integrate backward
y0B = [wrench;zeros(Nftot,1);zeros(6+Nftot,1);zeros(Nftot*(6+Nftot),1);reshape(eye(6),6*6,1);zeros(6*Nftot,1)];

% integration
funbackward = @(s,y) OdefunAssumeBackward(s,y,qei,wd,Nf,L,'variable');
[~,y] = ode45(funbackward,[1,0],y0B);
yforces = y(end,:)';

% extract results
Fc = yforces(1:6,1);
Qc =   yforces(1+6:6+Nftot,1);
dFcdLp = yforces(1+6+Nftot:6+Nftot+6,1);
dQcdLp = yforces(1+6+Nftot+6:6+Nftot+6+Nftot,1);
dFcdqe = reshape(yforces(1+6+Nftot+6+Nftot:6+Nftot+6+Nftot+6*Nftot,1),6,Nftot);
dQcdqe = reshape(yforces(1+6+Nftot+6+Nftot+6*Nftot:6+Nftot+6+Nftot+6*Nftot+Nftot*Nftot,1),Nftot,Nftot);
dFcdw0 = reshape(yforces(1+6+Nftot+6+Nftot+6*Nftot+Nftot*Nftot:6+Nftot+6+Nftot+6*Nftot+Nftot*Nftot+6*6,1),6,6);
dQcdw0 = reshape(yforces(1+6+Nftot+6+Nftot+Nftot*(6+Nftot)+6*6:6+Nftot+6+Nftot+Nftot*(6+Nftot)+6*(6+Nftot)),Nftot,6);
    
    
% platform equibribrium contributions
Rtip = quat2rotmatrix(hL);

Adg = [Rtip,zeros(3); zeros(3), Rtip]; % ONLY Rotate in global frame
gwrench = Adg*(-wrench); % the wrench is the opposite wrt the one of the beam
Adg2 = [eye(3),+skew(pp); zeros(3),eye(3)]; %  translate to platform origin

wrencheq = wrencheq + Adg2*gwrench; % equilibrium in platform frame
    
[D1,D2,D3] = derivativeColRotMatQuat(hL);
matr2 = [wrench(1:3,1)'*D1;wrench(1:3,1)'*D2;wrench(1:3,1)'*D3;wrench(4:6,1)'*D1;wrench(4:6,1)'*D2;wrench(4:6,1)'*D3;];
dgwrenchdLp = -matr2*dhLdLp;
dgwrenchdqe = -matr2*dhLdqe;
mat1 = [skew(dppdrot(:,1))*gwrench(4:6,1),skew(dppdrot(:,2))*gwrench(4:6,1),skew(dppdrot(:,3))*gwrench(4:6,1)];
dwrenchdqp = dwrenchdqp +[zeros(3,3),mat1;zeros(3,6)];
  
% equations
beameq(1+Nftot*6:Nftot*7,1) = L*Kee*qei + Qc;
dbeamdqe(1+Nftot*6:Nftot*7,1+Nftot*6:Nftot*7) = L*Kee + dQcdqe;
dbeamdlambd(1+Nftot*6:Nftot*7,1+5*6:5*7+1) = dQcdw0;

[angerr,dangerrdLp,dangerrdqe] = roterr(Rp,Rtip,hL,dhLdLp,dhLdqe);
dangerrdang = [invskew(dRpdroll'*Rtip-Rtip'*dRpdroll),invskew(dRpdpitch'*Rtip-Rtip'*dRpdpitch),invskew(dRpdyaw'*Rtip-Rtip'*dRpdyaw)];
geomconstr(1+5*6:5*7+1,1)  = [angerr;pL-pLp;];

dconsdqe(1+5*6:5*7+1,1+Nftot*6:Nftot*7) = [dangerrdqe;dpLdqe];
dconsdqp(1+5*6:5*7+1,:) = [ zeros(3,3),-dangerrdang;-eye(3),-dppdrot];
    
dwrenchdqe(:,1+Nftot*6:Nftot*7) = Adg2*dgwrenchdqe;
dwrenchdlambd(:,1+5*6:5*7+1) = -Adg2*Adg;

dbeamdLp(1+Nftot*6:Nftot*7,1) = Kee*qei + dQcdLp;
dconsdLp(1+5*6:5*7+1,1) = [dangerrdLp;dpLdLp];

dwrenchdLp(:,1) = Adg2*dgwrenchdLp;

Rbase = quat2rotmatrix(h0);
Adgbase = [Rbase,zeros(3);zeros(3),Rbase];
Fcglob = Adgbase*Fc;
Lpeq = Fcglob(6); 

dFcglobdqe = Adgbase*dFcdqe;
dLpeqdqe(1,1+6*Nftot:7*Nftot) = dFcglobdqe(6,:);
dFcglobdLp = Adgbase*dFcdLp;
dLpeqdLp = dFcglobdLp(6,1);
dFcglobdw0 = Adgbase*dFcdw0;
dLpeqdlambd(1,1+5*6:5*7+1) = dFcglobdw0(6,:);


%% COLLECT EQUATIONS
% ----------------------------------------------

% inverse problem

inv = qp-qpd;

% order to correct singu & stab evaluation
wrencheq = [wrencheq(4:6);wrencheq(1:3)];
dwrenchdqp = [dwrenchdqp(4:6,:);dwrenchdqp(1:3,:)];
dwrenchdlambd = [dwrenchdlambd(4:6,:);dwrenchdlambd(1:3,:)];
dwrenchdqa = [dwrenchdqa(4:6,:);dwrenchdqa(1:3,:)];
dwrenchdqe = [dwrenchdqe(4:6,:);dwrenchdqe(1:3,:)];
dwrenchdLp = [dwrenchdLp(4:6,1);dwrenchdLp(1:3,1)];
% collect

eq = [beameq;Lpeq;wrencheq;geomconstr;inv];

gradeq = [dbeamdqa,    dbeamdqe,          dbeamdLp,   zeros(7*Nftot,6),  dbeamdlambd;
          zeros(1,6),  dLpeqdqe,          dLpeqdLp,   zeros(1,6),        dLpeqdlambd;
          dwrenchdqa,  dwrenchdqe,        dwrenchdLp, dwrenchdqp,        dwrenchdlambd;
          dconsdqa,    dconsdqe,          dconsdLp,   dconsdqp,          zeros(5*7+1,7*5+1);
          zeros(6,6),  zeros(6,7*Nftot),  zeros(6,1), eye(6,6),          zeros(6,7*5+1);];
end