%% BACKWARD ODEs of a single beam
% for assumed strain mode approach
function yd = OdefunAssumeBackward(s,y,qe,~,Nf,L,str)

var = y(1:3);
mz = var(1);
nx = var(2);
ny = var(3);

ddqa = y(1+3+Nf:3+3+Nf,1);
dmzdqa = ddqa(1,1);
dnxdqa = ddqa(2,1);
dnydqa = ddqa(3,1);

ddqe = y(1+3+3+Nf+Nf:3+3+Nf+Nf+3*Nf,1);
dmzdqe = ddqe(1+0*Nf:1*Nf,1);
dnxdqe = ddqe(1+1*Nf:2*Nf,1);
dnydqe = ddqe(1+2*Nf:3*Nf,1);

ddw0 = y(1+3+Nf+3+Nf+(3+Nf)*Nf:+3+Nf+3+Nf+(3+Nf)*Nf+3*3,1);
dmzdw0 = ddw0(1+0*3:1*3,1);
dnxdw0 = ddw0(1+1*3:2*3,1);
dnydw0 = ddw0(1+2*3:3*3,1);
  
b = BaseFcnLegendre(s,1,Nf);
u = b'*qe;
        
%% these infos should be provided by forward integration
% backrotate in local frame the gravity
% not gravity also in derivatives!!!!!!!
wd = zeros(3,1);

%% 

switch str
    case 'fix'
        mzd = -ny-wd(1);
        nxd = +u*ny-wd(2);
        nyd = -u*nx-wd(3);
        Qcd = -b*mz;
        
        dmzdqad = -dnydqa;
        dmzdqed = -dnydqe;
        dmzdw0d = -dnydw0;
        
        dnxdqad = +u*dnydqa;
        dnxdqed = +b*ny+u*dnydqe;
        dnxdw0d = +u*dnydw0;
        
        dnydqad = -u*dnxdqa;
        dnydqed = -b*nx-u*dnxdqe;
        dnydw0d = -u*dnxdw0;
        
        dQcdqad = -b*dmzdqa';
        dQcdqed = -b*dmzdqe';
        dQcdw0d = -b*dmzdw0';
        
        yd = L*[mzd;nxd;nyd;Qcd;
            dmzdqad;dnxdqad;dnydqad;dQcdqad;
            dmzdqed;dnxdqed;dnydqed;reshape(dQcdqed,Nf*Nf,1);
            dmzdw0d;dnxdw0d;dnydw0d;reshape(dQcdw0d,3*Nf,1);];
    case 'variable'
        mzd = -L*(+ny+wd(1));
        nxd = +L*(+u*ny-wd(2));
        nyd = +L*(-u*nx-wd(3));
        Qcd = -L*b*mz;
        
        dmzdqad = -L*dnydqa - ny;
        dmzdqed = -L*dnydqe;
        dmzdw0d = -L*dnydw0;
        
        dnxdqad = +L*u*dnydqa + u*ny;
        dnxdqed = +L*(b*ny+u*dnydqe);
        dnxdw0d = +L*u*dnydw0;
        
        dnydqad = -L*u*dnxdqa - u*nx;
        dnydqed = -L*(+b*nx+u*dnxdqe);
        dnydw0d = -L*u*dnxdw0;
        
        dQcdqad = -L*b*dmzdqa -b*mz;
        dQcdqed = -L*b*dmzdqe';
        dQcdw0d = -L*b*dmzdw0';
        
        yd = [mzd;nxd;nyd;Qcd;
            dmzdqad;dnxdqad;dnydqad;dQcdqad;
            dmzdqed;dnxdqed;dnydqed;reshape(dQcdqed,Nf*Nf,1);
            dmzdw0d;dnxdw0d;dnydw0d;reshape(dQcdw0d,3*Nf,1);];
    otherwise
        error('Define settings for beams length');
end

end