function flag = StabilityModeSpatialRFRFR(jac,Nfm,y,rotparams)

Nf = 3*Nfm;
eul = y(1+2+2*Nf+3:2+2*Nf+6,1);
D = rot2twist(eul,rotparams);
jac(1+2*Nf+3:2*Nf+6,:) = D'*jac(1+2*Nf+3:2*Nf+6,:);

U = jac(1:2*Nf+6,1+2:2+2*Nf);
P = jac(1:2*Nf+6,1+2+2*Nf:2+2*Nf+6);

Z = null(jac(1:2*Nf+6,1+2+2*Nf+6:2+2*Nf+6+2*5+1)');

H = [U,P];
Hr = Z'*H*Z;

[~,flag] = chol(Hr);

end