%% Forward Geometrico Static Problem
% Strong form solved with shooting method
% RFRFR Robot


clear
close all
clc

%%
r = 0.001;
E = 210*10^9;
Lf = .7;
Lr = .3;
A = pi*r^2;
I = 0.25*pi*r^4;
Kbt = E*I;
g = [0;0];
rho = 7800;
f = rho*A*g;
fext = [0;.0];

wp = fext; % RFRFR no moments on EE
wd = [0;f]; % distributed wrench


%% Forward problem data

qad = [120;60]*pi/180;

%% GEOMETRY
rAB = 0.3;
ang_offset = pi/2;
basepoints = [-rAB/2,+rAB/2;0,0];
geometry.basepoints = basepoints;
geometry.angoffset = [+ang_offset;-ang_offset];

%% Solution
Nf = 4;
qa0 = qad;
qe0 = zeros(2*Nf,1);
qp0 = [0;0.7];
lambda0 = zeros(4,1);
guess0 = [qa0;qe0;qp0;lambda0];
options = optimoptions('fsolve','display','iter-detailed','Algorithm','trust-region','Maxiter',50,'SpecifyObjectiveGradient',true,'CheckGradients',true);

Phi = @(s) BaseFcnLegendre(s,1,Nf)';
Kad = integral(@(s) Phi(s)'*Kbt*Phi(s),0,1,'ArrayValued',true);

fun = @(guess) ForwardModeR_F_5R(guess,geometry,Kad,Lf,Lr,wp,wd,qad,Nf);
[sol,~,~,~,jac] = fsolve(fun,guess0,options);

%% PLOT
qa = sol(1:2,1);
qp = sol(1+2+2*Nf:2+2*Nf+2,1);
pplat = qp;

[pos1,pos2] = poseR_F_5R(sol,geometry,Lf,Lr,Nf);

h = PlotR_F_5R(pos1,pos2,geometry);
title('Assumed Mode')
axis equal
