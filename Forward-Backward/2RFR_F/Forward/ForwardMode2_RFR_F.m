function [eq,gradeq] = ForwardMode2_RFR_F(guess,geometry,L,Kee,qad,wp,wd,Nf)

basepoints = geometry.basepoints;
platpoints = geometry.platpoints;
rodbaseangle = geometry.rodbaseangle;

qa = guess(1:3,1);
qe = guess(1+3:3+3*Nf,1);
qp = guess(1+3+3*Nf:3+3*Nf+3,1);
lambda = guess(1+3+3*Nf+3:3+3*Nf+3+7,1);

pplat = qp(1:2,1);
thplat = qp(3,1);
Rp = Rz(thplat);
dRpdthp = Rz(thplat+pi/2);

Ci = [zeros(1,2);eye(2)];

%% BEAM 1 INTEGRATION
% extract variables
p01 = basepoints(:,1);
pp1 = Rp*platpoints(:,1);
pL1p = pplat +pp1;
th01 = qa(1,1);
qe1 = qe(1+Nf*(1-1):1*Nf,1);
lambda1 = lambda(1:2,1);
wrench1 = (Ci*lambda1);
L1 = L;
% integrate forward
y0F1 = [p01;th01;zeros(2+2*Nf,1);1;zeros(Nf,1)];
fun1 = @(s,y) OdefunAssumedForward(s,y,qe1,Nf,L1,'fix');
[~,y] = ode45(fun1,[0,1],y0F1);
ygeom = y(end,:)';
   
% extract results
pL1 = ygeom(1:2,1);
th1L = ygeom(3,1);
dpxdqeL = ygeom(1+5:5+Nf,1)';
dpydqeL = ygeom(1+5+Nf:5+2*Nf,1)';
dpL1dqa1 = ygeom(4:5,1);
dpL1dqe1 = [dpxdqeL;dpydqeL];
dth1Ldqa1 = ygeom(1+5+2*Nf,1);
dth1Ldqe1 = ygeom(1+5+2*Nf+1:end,1)'; 

% integrate backward
y0B1 = [wrench1;zeros(Nf,1);...
       zeros(3+Nf,1);...
       zeros(3*Nf+Nf*Nf,1);...
       reshape(eye(3),3*3,1);zeros(3*Nf,1)];

% integration
funbackward = @(s,y) OdefunAssumeBackward(s,y,qe1,wd,Nf,L1,'fix');
[~,y] = ode45(funbackward,[1,0],y0B1);
yforces = y(end,:)';
       
% extract results
Qc1 =   yforces(1+3:3+Nf,1);
dQc1dqa1 = yforces(1+3+Nf+3:3+Nf+3+Nf,1);
dQc1dqe1 = reshape(yforces(1+3+Nf+3+Nf+3*Nf:3+Nf+3+Nf+3*Nf+Nf*Nf,1),Nf,Nf);
dQc1dw01 = reshape(yforces(1+3+Nf+3+Nf+3*Nf+Nf*Nf+3*3:3+Nf+3+Nf+3*Nf+Nf*Nf+3*3+3*Nf,1),Nf,3);

% platform equibribrium contributions
Rtip1 = Rz(th1L);
dRtip1dth1 = Rz(th1L+pi/2);

n1 = -Rtip1*wrench1(2:3);
m1 = -wrench1(1) +n1(2)*pp1(1,1) -n1(1)*pp1(2,1);

dn1dqa1 = -dRtip1dth1*wrench1(2:3)*dth1Ldqa1;
dn1dqe1 = -dRtip1dth1*wrench1(2:3)*dth1Ldqe1;
dn1dlambd1 = -Rtip1;
dm1dqa1 = +dn1dqa1(2)*pp1(1,1) -dn1dqa1(1)*pp1(2,1);
dm1dqe1 = +pp1(1,1)*dn1dqe1(2,:) -pp1(2,1)*dn1dqe1(1,:);
dpp1dthp = dRpdthp*platpoints(:,1);
dm1dthp = +n1(2)*dpp1dthp(1,1) -n1(1)*dpp1dthp(2,1);
dm1dlambd1 = +pp1(1,1)*dn1dlambd1(2,:) -pp1(2,1)*dn1dlambd1(1,:);

%% BEAM 2 INTEGRATION
% extract variables
p02 = basepoints(:,2);
pp2 = Rp*platpoints(:,2);
pL2p = pplat + pp2;
th02 = rodbaseangle;
qe2 = qe(1+Nf*(2-1):2*Nf,1);
lambda2 = lambda(3:5,1);
wrench2 = (eye(3)*lambda2);
L2 = qa(2,1);

% integrate
y0F2 = [p02;th02;zeros(2+2*Nf,1);0;zeros(Nf,1)];
fun1 = @(s,y) OdefunAssumedForward(s,y,qe2,Nf,L2,'variable');
[~,y] = ode45(fun1,[0,1],y0F2);
ygeom = y(end,:)';

% extract results
pL2 = ygeom(1:2,1);
th2L = ygeom(3,1);
dpxdqeL = ygeom(1+5:5+Nf,1)';
dpydqeL = ygeom(1+5+Nf:5+2*Nf,1)';
dpL2dqa2 = ygeom(4:5,1);
dth2Ldqa2 = ygeom(1+5+2*Nf,1);
dpL2dqe2 = [dpxdqeL;dpydqeL];
dth2Ldqe2 = ygeom(1+5+2*Nf+1:end,1)'; 

% integrate backward
y0B2 = [wrench2;zeros(Nf,1);...
       zeros(3+Nf,1);...
       zeros(3*Nf+Nf*Nf,1);...
       reshape(eye(3),3*3,1);zeros(3*Nf,1)];

% integration
funbackward = @(s,y) OdefunAssumeBackward(s,y,qe2,wd,Nf,L2,'variable');
[~,y] = ode45(funbackward,[1,0],y0B2);
yforces = y(end,:)';
       
% extract results
Qc2 =   yforces(1+3:3+Nf,1);
dQc2dqa2 = yforces(1+3+Nf+3:3+Nf+3+Nf,1);
dQc2dqe2 = reshape(yforces(1+3+Nf+3+Nf+3*Nf:3+Nf+3+Nf+3*Nf+Nf*Nf,1),Nf,Nf);
dQc2dw02 = reshape(yforces(1+3+Nf+3+Nf+3*Nf+Nf*Nf+3*3:3+Nf+3+Nf+3*Nf+Nf*Nf+3*3+3*Nf,1),Nf,3);

% platform equibribrium contributions
Rtip2 = Rz(th2L);
dRtip2dth2 = Rz(th2L+pi/2);
n2 = -Rtip2*wrench2(2:3);
m2 = -wrench2(1) +n2(2)*pp2(1,1) -n2(1)*pp2(2,1);

dn2dqa2 = -dRtip2dth2*wrench2(2:3)*dth2Ldqa2;
dn2dqe2 = -dRtip2dth2*wrench2(2:3)*dth2Ldqe2;
dn2dlambd2 = [zeros(2,1),-Rtip2];
dm2dqa2 = +dn2dqa2(2)*pp2(1,1) -dn2dqa2(1)*pp2(2,1);
dm2dqe2 = +pp2(1,1)*dn2dqe2(2,:) -pp2(2,1)*dn2dqe2(1,:);
dpp2dthp = dRpdthp*platpoints(:,2);
dm2dthp = +n2(2)*dpp2dthp(1,1) -n2(1)*dpp2dthp(2,1);
dm2dlambd2 = -[1,0,0] +pp2(1,1)*dn2dlambd2(2,:) -pp2(2,1)*dn2dlambd2(1,:);

%% BEAM 3 INTEGRATION
% extract variables
p03 = basepoints(:,3);
pp3 = Rp*platpoints(:,3);
pL3p = pplat +pp3;
th03 = qa(3,1);
qe3 = qe(1+Nf*(3-1):3*Nf,1);
lambda3 = lambda(6:7,1);
wrench3 = (Ci*lambda3);
L3 = L;

% integrate forward
y0F3 = [p03;th03;zeros(2+2*Nf,1);1;zeros(Nf,1)];
fun1 = @(s,y) OdefunAssumedForward(s,y,qe3,Nf,L3,'fix');
[~,y] = ode45(fun1,[0,1],y0F3);
ygeom = y(end,:)';

% extract results
pL3 = ygeom(1:2,1);
th3L = ygeom(3,1);
dpxdqeL = ygeom(1+5:5+Nf,1)';
dpydqeL = ygeom(1+5+Nf:5+2*Nf,1)';
dpL3dqa3 = ygeom(4:5,1);
dth3Ldqa3 = ygeom(1+5+2*Nf,1);
dpL3dqe3 = [dpxdqeL;dpydqeL];
dth3Ldqe3 = ygeom(1+5+2*Nf+1:end,1)'; 


% integrate backward
y0B3 = [wrench3;zeros(Nf,1);...
       zeros(3+Nf,1);...
       zeros(3*Nf+Nf*Nf,1);...
       reshape(eye(3),3*3,1);zeros(3*Nf,1)];

% integration
funbackward = @(s,y) OdefunAssumeBackward(s,y,qe3,wd,Nf,L3,'fix');
[~,y] = ode45(funbackward,[1,0],y0B3);
yforces = y(end,:)';
       
% extract results
Qc3 =   yforces(1+3:3+Nf,1);
dQc3dqa3 = yforces(1+3+Nf+3:3+Nf+3+Nf,1);
dQc3dqe3 = reshape(yforces(1+3+Nf+3+Nf+3*Nf:3+Nf+3+Nf+3*Nf+Nf*Nf,1),Nf,Nf);
dQc3dw03 = reshape(yforces(1+3+Nf+3+Nf+3*Nf+Nf*Nf+3*3:3+Nf+3+Nf+3*Nf+Nf*Nf+3*3+3*Nf,1),Nf,3);

% platform equibribrium contributions
Rtip3 = Rz(th3L);
dRtip3dth3 = Rz(th3L+pi/2);

n3 = -Rtip3 *wrench3(2:3);
m3 = -wrench3(1) +n3(2)*pp3(1,1) -n3(1)*pp3(2,1);

dn3dqa3 = -dRtip3dth3*wrench3(2:3)*dth3Ldqa3;
dn3dqe3 = -dRtip3dth3*wrench3(2:3)*dth3Ldqe3;
dn3dlambd3 = -Rtip3;
dm3dqa3 = +dn3dqa3(2)*pp3(1,1) -dn3dqa3(1)*pp3(2,1);
dm3dqe3 = +pp3(1,1)*dn3dqe3(2,:) -pp3(2,1)*dn3dqe3(1,:);
dpp3dthp = dRpdthp*platpoints(:,3);
dm3dthp = +n3(2)*dpp3dthp(1,1) -n3(1)*dpp3dthp(2,1);
dm3dlambd3 = +pp3(1,1)*dn3dlambd3(2,:) -pp3(2,1)*dn3dlambd3(1,:);

%% EQUATIONS
% equilibrium equations
beameq = [L1*Kee*qe1 + Qc1; L2*Kee*qe2 + Qc2; L3*Kee*qe3 + Qc3;];

dbeamdqa = [dQc1dqa1,    zeros(Nf,1),        zeros(Nf,1);
              zeros(Nf,1), Kee*qe2 + dQc2dqa2, zeros(Nf,1);
              zeros(Nf,1), zeros(Nf,1),        dQc3dqa3;];
          
dbeamdqe = [L1*Kee + dQc1dqe1, zeros(Nf,Nf),      zeros(Nf,Nf);
              zeros(Nf,Nf),      L2*Kee + dQc2dqe2, zeros(Nf,Nf);
              zeros(Nf,Nf),      zeros(Nf,Nf),      L3*Kee + dQc3dqe3;];
          
dbeamdlambd = [dQc1dw01*Ci, zeros(Nf,3),     zeros(Nf,2);
                 zeros(Nf,2), dQc2dw02*eye(3), zeros(Nf,2);
                 zeros(Nf,2), zeros(Nf,3),     dQc3dw03*Ci];

% geometric constraints
geomconstr = [pL1-pL1p; pL2-pL2p; th2L-(thplat+pi/2); pL3-pL3p;];

dconsdqa = [dpL1dqa1,   zeros(2,1), zeros(2,1);
            zeros(2,1), dpL2dqa2,   zeros(2,1);
            zeros(1,1), dth2Ldqa2,  zeros(1,1);
            zeros(2,1), zeros(2,1), dpL3dqa3];
        
dconsdqe = [dpL1dqe1,    zeros(2,Nf), zeros(2,Nf);
            zeros(2,Nf), dpL2dqe2,    zeros(2,Nf);
            zeros(1,Nf), dth2Ldqe2,   zeros(1,Nf);
            zeros(2,Nf), zeros(2,Nf), dpL3dqe3];
        
dconsdqp = -[eye(2),dRpdthp*platpoints(:,1);
             eye(2),dRpdthp*platpoints(:,2);
             zeros(1,2),1;
             eye(2),dRpdthp*platpoints(:,3);];
         
% platform equilibrium
force_eq = n1 + n2 + n3 + wp(2:3);
momen_eq = wp(1) + m1 + m2 + m3;
equilibrium = [force_eq;momen_eq];

dforcedqa = [dn1dqa1,dn2dqa2,dn3dqa3];
dforcedqe = [dn1dqe1,dn2dqe2,dn3dqe3];
dforcedqp = zeros(2,3);
dforcedlambd = [dn1dlambd1,dn2dlambd2,dn3dlambd3];
dmomdqa = [dm1dqa1,dm2dqa2,dm3dqa3];
dmomdqe = [dm1dqe1,dm2dqe2,dm3dqe3];
dmomdqp = [zeros(1,2), dm1dthp+dm2dthp+dm3dthp];
dmomdlambd = [dm1dlambd1,dm2dlambd2,dm3dlambd3];

dequidqa = [dforcedqa;dmomdqa];
dequidqe = [dforcedqe;dmomdqe];
dequidqp = [dforcedqp;dmomdqp];
dequidlambd = [dforcedlambd;dmomdlambd];

% inverse problem

forw = qa-qad;

% collect

eq = [beameq;equilibrium;geomconstr;forw];

gradeq = [dbeamdqa,   dbeamdqe,      zeros(3*Nf,3), dbeamdlambd;
          dequidqa,   dequidqe,      dequidqp,      dequidlambd;
          dconsdqa,   dconsdqe,      dconsdqp,      zeros(7,7);
          eye(3,3),   zeros(3,3*Nf), zeros(3,3),      zeros(3,7);];
end