function flag = StabilityMode6RFS(jac,Nfm,eul,rotparams)
Nf = 3*Nfm;

D = rot2twist(eul,rotparams);

jac(1+6*Nf+3:6*Nf+6,:) = D'*jac(1+6*Nf+3:6*Nf+6,:);
U = jac(1:6*Nf+6,1+6:6+6*Nf);
P = jac(1:6*Nf+6,1+6+6*Nf:6+6*Nf+6);
G = jac(1:6*Nf+6,1+6+6*Nf+6:6+6*Nf+6+18);
Z = null(G');

H = [U,P];
Hr = Z'*H*Z;
[~,flag] = chol(Hr);

end