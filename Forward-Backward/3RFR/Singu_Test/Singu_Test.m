%%
r = 0.001;
E = 210*10^9;
L = 1;
A = pi*r^2;
I = 0.25*pi*r^4;
EI = E*I;
Kbt = E*I;
g = [0;0];
rho = 7800;
f = rho*A*g;
fext = [0;0];
mext = 0;
wp = [mext;fext];
wd = [0;f]; % distributed wrench


%% Inverse problem data

th = 0*pi/180;
pp = [0;0];
qpd = [pp;th];

%% GEOMETRY
rB = 0.60;   % base radius [m]
rP = 0.15;   % platform radius [m]

th1 = pi/2; % wrt fixed frame 
th2 = pi/2+2*pi/3;
th3 = pi/2-2*pi/3;

A1 = rB*[cos(th1);sin(th1)]; % wrt fixed frame 
A2 = rB*[cos(th2);sin(th2)]; 
A3 = rB*[cos(th3);sin(th3)];

B1 = rP*[cos(th1);sin(th1)]; % wrt platform frame 
B2 = rP*[cos(th2);sin(th2)]; 
B3 = rP*[cos(th3);sin(th3)];

basepoints = [A1,A2,A3];
platformpoints = [B3,B1,B2];
geometry.basepoints = basepoints;
geometry.platpoints = platformpoints;


%% Solution
Nf = 4;
qa0 = [0;120;240]*pi/180;
qe0 = zeros(3*Nf,1);
qp0 = [pp;th];
lambda0 = zeros(6,1);
guess0 = [qa0;qe0;qp0;lambda0];
options = optimoptions('fsolve','display','iter-detailed','Maxiter',20,'SpecifyObjectiveGradient',true,'CheckGradients',true);

Phi = @(s) BaseFcnLegendre(s,1,Nf)';
Kad = integral(@(s) Phi(s)'*EI*Phi(s),0,1,'ArrayValued',true);

fun = @(guess) InverseMode3RFR(guess,geometry,L,Kad,qpd,wp,wd,Nf);

[sol,~,flag,~,jac] = fsolve(fun,guess0,options);


%% PLOT & SHAPE RECOVERY
[pos1,pos2,pos3] = pos3RFR(sol,geometry,Nf,L);
pplat = sol(1+3+3*Nf:3+3*Nf+2,1);
th = sol(3+3*Nf+3,1);
h = Plot3RFR(pos1,pos2,pos3,pplat,th,geometry,L);

%% MOVE
options = optimoptions('fsolve','display','off','Algorithm','trust-region','Maxiter',10,'SpecifyObjectiveGradient',true,'CheckGradients',false);

i = 1;
flag = 1;
i_max = 1000;
while i<i_max && flag>0
    guess0 = sol;
    qpd = qpd-[0;0;1*pi/180];
    fun = @(guess) InverseMode3RFR(guess,geometry,L,Kad,qpd,wp,wd,Nf);
    [sol,~,flag,~,jac] = fsolve(fun,guess0,options);
    jac = full(jac);
    yy(i) = qpd(3);
    flags(i) = StabilityMode3RFR(jac,Nf);
    [t1,t2] = SingularityMode3RFR(jac,Nf);
    flagt1(i) = t1;
    flagt2(i) = t2;
    i = i+1
    
    [pos1,pos2,pos3] = pos3RFR(sol,geometry,Nf,L);
    pplat = sol(1+3+3*Nf:3+3*Nf+2,1);
    th = sol(3+3*Nf+3,1);
    pause(0.01)
    delete(h)
    h = Plot3RFR(pos1,pos2,pos3,pplat,th,geometry,L);
    
end

%% RESULTS
figure()
subplot(1,2,1)
yyaxis left
plot(yy,flags)
yyaxis right
semilogy(yy,flagt2)
grid on
subplot(1,2,2)
semilogy(yy,flagt1)
grid on