%% Singularity Test
% Assumed strain mode approach - Forward/Backward formulation
% 6 RFS robot

% 14 Dec 2021 - Federico Zaccaria

clear
close all
clc
%%
r = 0.001;
E = 210*10^9;
G = 80*10^9;
L = 1;
A = pi*r^2;
I = 0.25*pi*r^4;
J = 2*I;
EI = E*I;
GJ = G*J;
Kbt = diag([EI;EI;GJ]);
g = [0;0;0];
rho = 7800;
f = rho*A*g;
fext = [0;0;0];
mext = [0;0;0];
wp = [mext;fext];
wd = [zeros(3,1);f]; % distributed wrench


%% Inverse problem data
rotparams = 'TT';
roll = 90*pi/180;
pitch = 20*pi/180;
yaw = 0*pi/180;

pp = [0;0;0.82];
qpd = [pp;roll;pitch;yaw];

%% GEOMETRY

rb = 0.4;
rp = 0.2;
db = 0.1;
dp = 0.05;

th1 =   0*pi/180;
th2 = 120*pi/180;
th3 = 240*pi/180;
th4 =  30*pi/180;
th5 = 150*pi/180;
th6 = 270*pi/180;

angles = [th1,th2,th3];

b1 = Rz(th1)*[0;rb;0]; % WRT base frame
b2 = Rz(th2)*[0;rb;0];
b3 = Rz(th3)*[0;rb;0];

b1A1 = +Rz(th1)*[db;0;0];
b1A2 = -Rz(th1)*[db;0;0];
b2A3 = +Rz(th2)*[db;0;0];
b2A4 = -Rz(th2)*[db;0;0];
b3A5 = +Rz(th3)*[db;0;0];
b3A6 = -Rz(th3)*[db;0;0];

A1 = b1 +b1A1;
A2 = b1 +b1A2;
A3 = b2 +b2A3;
A4 = b2 +b2A4;
A5 = b3 +b3A5;
A6 = b3 +b3A6;

basepoints = [A1,A2,A3,A4,A5,A6];

p1 = Rz(th4)*[rp;0;0]; % WRT platform frame
p2 = Rz(th5)*[rp;0;0];
p3 = Rz(th6)*[rp;0;0];

p1B1 = +Rz(th4)*[0;dp;0];
p1B2 = -Rz(th4)*[0;dp;0];
p2B3 = +Rz(th5)*[0;dp;0];
p2B4 = -Rz(th5)*[0;dp;0];
p3B5 = +Rz(th6)*[0;dp;0];
p3B6 = -Rz(th6)*[0;dp;0];

B1 = p1+p1B1;
B2 = p1+p1B2;
B3 = p2+p2B3;
B4 = p2+p2B4;
B5 = p3+p3B5;
B6 = p3+p3B6;

platformpoints = [B1,B4,B3,B6,B5,B2];

baseangles = [angles(1);angles(1);angles(2);angles(2);angles(3);angles(3)];

geometry.basepoints = basepoints;
geometry.platpoints = platformpoints;
geometry.baseangles = baseangles;
geometry.rotparams = rotparams;

%% Solution

Nf = 4;
% qa0 = zeros(6,1);
% A0 = [1;zeros(3*Nf-1,1)];
% qe0 = [+A0;+A0;+A0;+A0;+A0;+A0;];
% qp0 = [pp;roll;pitch;yaw];
% lambda0 = zeros(3*6,1);
% guess0 = [qa0;qe0;qp0;lambda0];
load sol_hole
guess0 = sol;
options = optimoptions('fsolve','display','iter-detailed','Maxiter',50,'SpecifyObjectiveGradient',true,'CheckGradients',false);

M = @(s) PhiMatr(s,1,Nf);

Kad = integral(@(s) M(s)'*Kbt*M(s),0,1,'ArrayValued',true);

fun = @(guess) InverseMode6RFS(guess,geometry,L,Kad,qpd,wp,wd,Nf);

[sol,~,flag,~,jac] = fsolve(fun,guess0,options);

%% PLOT & SHAPE RECOVERY
posbeams = pos6RFS(sol,geometry,Nf,L);

qa = sol(1:6,1);
pplat = sol(1+6+6*3*Nf:6+6*3*Nf+3,1);
[Rplat,~,~,~] = rotationParametrization(sol(1+6+6*3*Nf+3:6+6*3*Nf+6,1),rotparams);

h = Plot6RFS(geometry,qa,posbeams,pplat,Rplat);

% MOVE
options = optimoptions('fsolve','display','off','Algorithm','trust-region','Maxiter',5,'SpecifyObjectiveGradient',true,'CheckGradients',false);
i = 1;
i_max = 20;
while i<i_max && flag>0
    guess0 = sol;
    qpd = qpd-[0;0;0;0;1*pi/180;0];
    fun = @(guess) InverseMode6RFS(guess,geometry,L,Kad,qpd,wp,wd,Nf);
    [sol,~,flag,~,jac] = fsolve(fun,guess0,options);
    jac = full(jac);
    yy(i) = i;
    flags(i) = StabilityMode6RFS(jac,Nf,qpd(4:6),rotparams);
    [t1,t2] = SingularityMode6RFS(jac,Nf,qpd(4:6),rotparams);
    flagt1(i) = t1;
    flagt2(i) = t2;
    invjac(i) = norm(inv(jac),Inf);
    i = i+1    

end

posbeams = pos6RFS(sol,geometry,Nf,L);
qa = sol(1:6,1);
pplat = sol(1+6+6*3*Nf:6+6*3*Nf+3,1);
[Rplat,~,~,~] = rotationParametrization(sol(1+6+6*3*Nf+3:6+6*3*Nf+6,1),rotparams);
pause(0.001)
delete(h)
h = Plot6RFS(geometry,qa,posbeams,pplat,Rplat);
%% RESULTS
figure()
subplot(1,2,1)
yyaxis left
plot(yy,flags)
yyaxis right
semilogy(yy,flagt2)
grid on
subplot(1,2,2)
yyaxis left
semilogy(yy,flagt1)
yyaxis right
semilogy(yy,invjac)
grid on