%% Forward geometrico static problem 6RFR robot
% assumed strain mode approach, forward-backward formulation
% Federico Zaccaria 05 Jan 2022

clear
close all
clc
%%
r = 0.001;
E = 210*10^9;
G = 80*10^9;
L = 1;
A = pi*r^2;
I = 0.25*pi*r^4;
J = 2*I;
EI = E*I;
GJ = G*J;
Kbt = diag([EI;EI;GJ]);
g = [0;0;-9.81];
rho = 0;
f = rho*A*g;
mp = 0;
fext = [0;0;0] + mp*g;
wp = [zeros(3,1);fext];
wd = [zeros(3,1);f]; % distributed wrench

%% Forward problem data
rotparams = 'XYZ';
qad = 30*ones(6,1)*pi/180;

%% GEOMETRY
rb = 0.2;
rp = 0.2;
dp = 0.1; % distance in platform frame
alpha = 30*pi/180;

th = [0,120,240]*pi/180;
triangleplat = rp*[cos(th);sin(th);zeros(1,3)];
P1 = triangleplat + dp/2*[cos(th+pi/2);sin(th+pi/2);zeros(1,3)];
P2 = triangleplat + dp/2*[cos(th-pi/2);sin(th-pi/2);zeros(1,3)];

platpoints = [P1(:,1),P2(:,2),P1(:,2),P2(:,3),P1(:,3),P2(:,1)];
jointangles = pi/2 + [th(1),th(2),th(2),th(3),th(3),th(1)];

B1 = rb*[cos(th+alpha);sin(th+alpha);zeros(1,3)];
B2 = rb*[cos(th-alpha);sin(th-alpha);zeros(1,3)];

basepoints = [B1(:,1),B2(:,2),B1(:,2),B2(:,3),B1(:,3),B2(:,1)];
baseangles = pi/2 + [th(1)+alpha,th(2)-alpha,th(2)+alpha,th(3)-alpha,th(3)+alpha,th(1)-alpha];

geometry.platpoints = platpoints;
geometry.jointangles = jointangles;

geometry.basepoints = basepoints;
geometry.baseangles = baseangles;

geometry.rotparams = rotparams;

%% Solution
Nf = 4;
qa0 = qad;
qe0 = repmat([0;zeros(3*Nf-1,1)],6,1);
qp0 = [0;0;0.8;0;0;0];
lambda0 = zeros(6*5,1);
guess0 = [qa0;qe0;qp0;lambda0];
options = optimoptions('fsolve','Algorithm','trust-region','display','iter-detailed','SpecifyObjectiveGradient',true,'CheckGradients',true);

M = @(s) PhiMatr(s,1,Nf);

Kad = integral(@(s) M(s)'*Kbt*M(s),0,1,'ArrayValued',true);

fun = @(guess) ForwardMode6RFR(guess,geometry,L,Kad,qad,wp,wd,Nf);

[sol,~,flag,~,jac] = fsolve(fun,guess0,options);

%% PLOT & SHAPE RECOVERY
posbeams = pos6RFR(sol,geometry,Nf,L);

qa = sol(1:6,1);
pplat = sol(1+6+6*3*Nf:6+6*3*Nf+3,1);
[Rplat,~,~,~] = rotationParametrization(sol(1+6+6*3*Nf+3:6+6*3*Nf+6,1),rotparams);

h = Plot6RFR(geometry,qa,posbeams,pplat,Rplat);
drawnow
