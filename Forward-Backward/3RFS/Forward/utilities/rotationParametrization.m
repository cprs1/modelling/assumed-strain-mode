% ROTATION MATRIX AND ITS DERIVATIVES wrt its angles

function [Rp,dRpda,dRpdb,dRpdc] = rotationParametrization(ang,str)
a = ang(1);
b = ang(2);
c = ang(3);

switch str
    case 'ZYZ'
        Rp = Rz(a)*Ry(b)*Rz(c);
        dRpda = dRzdt(a)*Ry(b)*Rz(c);
        dRpdb = Rz(a)*dRydt(b)*Rz(c);
        dRpdc  = Rz(a)*Ry(b)*dRzdt(c);
    case 'XYZ'
        Rp = Rx(a)*Ry(b)*Rz(c);
        dRpda = dRxdt(a)*Ry(b)*Rz(c);
        dRpdb = Rx(a)*dRydt(b)*Rz(c);
        dRpdc  = Rx(a)*Ry(b)*dRzdt(c);
    case 'TT'
        Rp = Rz(a)*Ry(b)*Rz(-a)*Rz(c);
        dRpda = dRzdt(a)*Ry(b)*Rz(-a)*Rz(c) - Rz(a)*Ry(b)*dRzdt(-a)*Rz(c);
        dRpdb = Rz(a)*dRydt(b)*Rz(-a)*Rz(c);
        dRpdc = Rz(a)*Ry(b)*Rz(-a)*dRzdt(c);
    otherwise
        error('Define orientation kind')
end

    
end