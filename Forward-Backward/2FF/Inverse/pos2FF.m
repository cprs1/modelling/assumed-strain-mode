function [pos1,pos2] = pos2FF(guess,geometry,Nf)
Nsh = 100;
basepoints = geometry.basepoints;
rodbaseangle = geometry.rodbaseangle;

qa = guess(1:2,1);
qe = guess(1+2:2+2*Nf,1);

% extract variables
p01 = basepoints(:,1);
th01 = rodbaseangle;
L1 = qa(1);
qe1 = qe(1+Nf*(1-1):1*Nf,1);
% integrate
y01 = [p01;th01];
fun1 = @(s,y) OdeFunReconstruct(s,y,qe1,Nf,L1);
[s1,y1] = ode45(fun1,[0,1],y01);

% spline results
sspan1 = linspace(0,L1,Nsh);
pos1 = spline(L1*s1,y1(:,1:2)',sspan1);


%% BEAM 2 INTEGRATION
% extract variables
p02 = basepoints(:,2);
th02 = rodbaseangle;
L2 = qa(2);
qe2 = qe(1+Nf*(2-1):2*Nf,1);
% integrate
y02 = [p02;th02];
fun1 = @(s,y) OdeFunReconstruct(s,y,qe2,Nf,L2);
[s2,y2] = ode45(fun1,[0,1],y02);

% spline results
sspan2 = linspace(0,L2,Nsh);
pos2 = spline(L2*s2,y2(:,1:2)',sspan2);

end