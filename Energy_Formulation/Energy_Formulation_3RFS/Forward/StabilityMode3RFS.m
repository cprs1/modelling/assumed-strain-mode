function flag = StabilityMode3RFS(jac,Nfm)
Nf = 3*Nfm;


U_el =   jac(1:3*Nf+6,1+3     :3+3*Nf);
P_full = jac(1:3*Nf+6,1+3+3*Nf:3+3*Nf+6);

% P = P_full(:,1:3);
% P_orient = P_full(:,4:6);
% U = [U_el,P_orient];
P = P_full;

G = jac(1:3*Nf+6,1+3+3*Nf+6:3+3*Nf+6+3*3);
Z = null(G');

H = [U,P];
Hr = Z'*H*Z;

[~,flag] = chol(Hr);

end