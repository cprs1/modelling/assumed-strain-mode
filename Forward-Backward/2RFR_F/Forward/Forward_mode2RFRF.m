%% 2-RFR-F robot
% forward problem with assumed mode

clear
close all
clc

%% GEOMETRICAL PARAMETERS

% base
rAB = 0.5;
A1 = [-rAB/2;0];
A2 = [0;+.10];
A3 = [+rAB/2;0];
basepoints = [A1,A2,A3];

% platform
lP = 0.3;
B1 = [-lP/2;0];
B2 = [0;+0.1];
B3 = [+lP/2;0];
platpoints = [B1,B2,B3];

rodbaseangle = +pi/2;

geometry.basepoints = basepoints;
geometry.platpoints = platpoints;
geometry.rodbaseangle = rodbaseangle;

%% MATERIAL PARAMETERS
r = 0.001;
E = 210*10^9;
I = 0.25*pi*r^4;
L = 1; % for external beams
EI = E*I;
sigmamax = +Inf;

%% EXTERNAL LOADS
fd = [0;0];
fend = [0;0];
mend = 0;
wp = [mend;fend];
wd = [0;fend];

%% FORWARD PROBLEM

qad = [110*pi/180;0.75;-20*pi/180];

%% SIMULATION
Nf = 4;
Phi = @(s) BaseFcnLegendre(s,1,Nf)';
Kad = integral(@(s) Phi(s)'*EI*Phi(s),0,1,'ArrayValued',true);

qa0 = qad;
qe0 = [0;zeros(Nf-1,1);0;zeros(Nf-1,1);0;zeros(Nf-1,1);];
qp0 = [0;0.8;0];
lambda0 = 0.00001*rand(2*2+3,1);
guess0 = [qa0;qe0;qp0;lambda0];

fun = @(guess) ForwardMode2_RFR_F(guess,geometry,L,Kad,qad,wp,wd,Nf);

options = optimoptions('fsolve','SpecifyObjectiveGradient',true,'CheckGradients',true,'Display','iter-detailed','Maxiter',50,'Algorithm','trust-region');
[sol,~,flag,~,jac] = fsolve(fun,guess0,options);

%% PLOT & SHAPE RECOVERY
[pos1,pos2,pos3] = pos2_RFR_F(sol,geometry,Nf,L);
pplat = sol(1+3+3*Nf:3+3*Nf+2,1);
th = sol(3+3*Nf+3,1);
Plot2_RFR_F(pos1,pos2,pos3,pplat,th,geometry,L)
