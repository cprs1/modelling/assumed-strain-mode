% convert derivative of angles into twist (local)
function M = rot2twist(ang,str)
switch str
    case 'XYZ'
        % R = Rx(a) Ry(b) Rz(g)
        % twist = M [a;b;g]'
%         a = ang(1);
        b = ang(2);
        g = ang(3);
        M = [+cos(g)*cos(b),+sin(g),0;-cos(b)*sin(g),cos(g),0;+sin(b),0,1];
    case 'ZYZ'
        % R = Rz(a) Ry(b) Rz(g)
        % twist = M [a;b;g]'
%         a = ang(1);
        b = ang(2);
        g = ang(3);
        M = [-cos(g)*sin(b),sin(g),0;+sin(b)*sin(g),cos(g),0;+cos(b),0,1];
    case 'quat'
        % R = R(quaternion)
        % [twist;0] = M [h1;h2;h3;h4]' (h1 = scalar component)
        h1 = ang(1);
        h2 = ang(2);
        h3 = ang(3);
        h4 = ang(4);
        M = [+h2,-h1,+h4,-h3;+h3,-h4,-h1,+h2;+h4,+h3,-h2,-h1;+h1,+h2,+h3,+h4];
end
end