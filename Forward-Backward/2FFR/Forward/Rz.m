function M = Rz(th)
M = [+cos(th),-sin(th);
     +sin(th),+cos(th)];
end