function posbeams = posSpatialRFRFR(guess,geometry,Nf,L)
Nsh = 100;
basepoints = geometry.basepoints;
qa = guess(1:2,1);
qe = guess(1+2:2+2*3*Nf,1);

posbeams = zeros(2*3,Nsh);

for i = 1:2
    % extract variables
    p01 = basepoints(:,i);
    h01 = eul2quat([0,0,qa(i)],'ZYX')';
    qe1 = qe(1+3*Nf*(i-1):i*3*Nf,1);
    % integrate
    y01 = [p01;h01];
    fun1 = @(s,y) OdeFunReconstruct(s,y,qe1,Nf,L);
    [s1,y1] = ode45(fun1,[0,1],y01);

    % spline results
    sspan1 = linspace(0,L,Nsh);
    posbeams(1+3*(i-1):3*i,:) = spline(L*s1,y1(:,1:3)',sspan1);
end

end