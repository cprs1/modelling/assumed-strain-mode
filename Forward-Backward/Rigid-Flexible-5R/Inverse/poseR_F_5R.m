function [pos1,pos2] = poseR_F_5R(sol,geometry,Lf,Lr,Nf)
basepoints = geometry.basepoints;
ang_offset = geometry.angoffset;

Nsh = 100;
s_span = linspace(0,Lf,Nsh);

qa = sol(1:2);
qe =  sol(1+2:2+2*Nf,1);

%% BEAM 1
qa1 = qa(1);
qe1 = qe(1+Nf*(1-1):Nf*1,1);
fun = @(s,y) OdeFunReconstruct(s,y,qe1,Nf,Lf);
p0 = basepoints(:,1);
p1 = p0 + Rz(qa1)*[Lr;0];
th1 = qa1-ang_offset(1);
y0 = [p1;th1];
[si,yi] = ode45(fun,[0,1],y0);
pos1 = spline(Lf*si,yi(:,1:2)',s_span);

%% BEAM 2
qa2 = qa(2);
qe2 = qe(1+Nf*(2-1):Nf*2,1);
fun = @(s,y) OdeFunReconstruct(s,y,qe2,Nf,Lf);
p0 = basepoints(:,2);
p1 = p0 + Rz(qa2)*[Lr;0];
th1 = qa2-ang_offset(2);
y0 = [p1;th1];
[si,yi] = ode45(fun,[0,1],y0);
pos2 = spline(Lf*si,yi(:,1:2)',s_span);
end